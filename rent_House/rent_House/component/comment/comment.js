const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties:{
    commentData:{
      type:Array,
      value:[],
      observer(newVal, oldVal, changedPath) {
        // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
        // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        // console.log(this);
        // if (this.data.total) {
        //   let arr = [];
        //   console.log(this.data.total);
        //   for (let i = 0; i < this.data.total; i++) {
        //     arr.push({ status: false, showMore: false, maxCount: 2 });
        //   }
        //   console.log(arr);
        //   this.setData({
        //     showComment: arr
        //   })
        // }
      }
    },
    total: {
      type:Number,
      value:0
    },
    haveStar: {
      type: Boolean,
      value:false
    },
    haveReply: {
      type:Boolean,
      value:false
    },
    otype:{
      type:String,
      value:"3"
    },
    staticPage:{
      type:Array,
      value:[]
    },
    commentType: {
      type:Number,
      value:0
    }
  },
  data:{
    host:"https://rental.homhere.cn",
    showComment:[],
    textareaContent:'',
    commentCount:5,
    showMore:false,
    page:1
  },
  methods:{
    // 显示回复弹框
    _showReply(event) {
      let arr = this.data.showComment;
      arr[event.currentTarget.dataset.index].status = !arr[event.currentTarget.dataset.index].status
      this.setData({
        showComment:arr
      })
    },
    // 跳转到详情
    // _getDetail (event) {
    //   console.log('2',event);
    //   wx.navigateTo({
    //     url: '/pages/rentOut/rentOut?id=' + event.currentTarget.dataset.oid,
    //   })
    // },
    // _getUserInfo(event) {
    //   console.log('1', event);
    //   wx.navigateTo({
    //     url: '/pages/userInfo/userInfo?userid=' + event.currentTarget.dataset.oid,
    //   })
    // },
    // 弹出面板
    showSheet(event) {
      wx.showActionSheet({
        itemList: ['查看详情','删除'],
        success:res=> {
          if(res.tapIndex == 0){
            if(this.data.commentType == 1){
              wx.navigateTo({
                url: '/pages/userInfo/userInfo?userid=' + event.currentTarget.dataset.oid,
              })
            }else {
              wx.navigateTo({
                url: '/pages/rentOut/rentOut?id=' + event.currentTarget.dataset.oid,
              })
            }
          }
          else {
            wx.showModal({
              title: '温馨提示',
              content: '是否删除该评论？',
              success: result=> {
                if(result.confirm) {
                  this.deleteComment(event.currentTarget.id);
                }
              }
            })
          }
        } 
      })
    },
    // 删除评论
    deleteComment (id) {
      wxPromise.wxPost(API.USERRENTALDELETE_COMMENT,
        { id },
        wx.getStorageSync('Token')
      ).then(res => {
        if (res.data.code == 1) {
          wx.showToast({
            title: res.data.msg,
            success: () => {
              this._callback();
            }
          })
        } else if (res.data.code == 10001) {
          toast.toast(res.data.msg);
        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg
          })
        }
      })
    },
    // 渲染完成
    readyComplete() {
      let arr = [];
      for (let i = 0; i < this.data.total; i++) {
        arr.push({ status: false, showMore: false, maxCount: 2 });
      }
      this.setData({
        showComment: arr
      })
    },
    // 提交表单
    submitForm(event) {
      let flag = true;
      let warn = '';
      if (event.detail.value.content == '') {
        warn = '请填写内容'
      }
      else {
        flag = false;
        wx.showModal({
          title: '温馨提示',
          content: '确定提交？',
          success: result => {
            if (result.confirm) {
              const data = {
                content: event.detail.value.content,
                type: 2,
                otype: 4,
                oid: event.currentTarget.id,
              }
              this.comment(data);
            }
          }
        })
      }
      if (flag) {
        wx.showToast({
          title: warn,
          icon: 'none'
        })
      }
    },
    // 评论接口
    comment(data) {
      wx.showLoading({
        title: '',
      })
      wxPromise.wxPost(API.USERRENTALCOMMENT, data, wx.getStorageSync('Token'))
      .then(res => {
        wx.hideLoading();
        if (res.data.code == 1) {
          wx.showToast({
            title: '评论成功',
            icon: 'none'
          })
          this._callback();
          this.setData({
            textareaContent: '',
          })
        }
        else {
          toast.toast(res.data.msg);
        }
      })
    },
    // 加载更多评论
    moreComment (event) {
      let showComment = this.data.showComment;
      if(showComment[event.currentTarget.dataset.index].showMore) {
        showComment[event.currentTarget.dataset.index].showMore = !showComment[event.currentTarget.dataset.index].showMore
        showComment[event.currentTarget.dataset.index].maxCount = 2
        this.setData({
          showComment,
        })
      }else {
        showComment[event.currentTarget.dataset.index].showMore = !showComment[event.currentTarget.dataset.index].showMore
        showComment[event.currentTarget.dataset.index].maxCount = this.data.commentData[event.currentTarget.dataset.index].reply.length,
        this.setData({
          showComment
        })
      }
    },
    // 显示所有评论
    showAllComment(event) {
      this.setData({
        showMore : !this.data.showMore
      })
      if(this.data.showMore) {
        this.setData({
          commentCount : this.data.commentData.length
        })
      }else {
        this.setData({
          commentCount: 5
        })
      }
    },

    // 分页器

    // prev 上一页
    prev() {
      if (this.data.page <= 1) return;
      this.setData({
        page: this.data.page - 1
      })
      this._page(this.data.page);
    },
    // 选中哪一页
    selectPage(event) {
      this.setData({
        page: event.currentTarget.dataset.index
      })
      this._page(this.data.page);
    },
    // 下一页
    next() {
      if (this.data.page >= this.data.staticPage.length) return;
      this.setData({
        page: this.data.page + 1
      })
      this._page(this.data.page);
    },


    // 定义一个回调函数
    _callback() {
      this.triggerEvent('comment');
    },
    // 分页
    _page(page = 1) {
      this.triggerEvent('pageEvent', { page: page });
    },
    _readyComplete() {
      this.triggerEvent('readyEvent');
    }
  },  
  // 组件布局完成
  ready() {
    if (this.data.haveReply) {
      this.setData({
        commentCount:this.data.total
      })
    }
    if(this.data.total){
      let arr = [];
      for(let i = 0;i<this.data.total;i++){
        arr.push({status:false,showMore:false,maxCount:2});
      }
      this.setData({
        showComment:arr
      })
    }
    this._readyComplete();
  },
})