const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const app = getApp();
Component({
  properties:{
    houseResource:{
      type:Array,
      value:[],
    },
    haveLoading:{
      type:Boolean,
      value:false
    },
    haveFire: {
      type:Boolean,
      value:false
    }, 
    haveTime: {
      type:Boolean,
      value:false
    },
    isShow:{
      type:Boolean,
      value:false
    },
    haveCollection:{
      type:Boolean,
      value:false
    },
    type: {
      type:String,
      value:''
    }
  },
  data:{
    host:"https://rental.homhere.cn",
  },
  methods:{
    // 进入房源信息
    getHouseInfo(event) {
      let formid = event.detail.formId
      app.globalData.formId.push(formid)
      
      if (!this.data.isShow&&event.currentTarget.id) {
        wx.navigateTo({
          url: '/pages/rentOut/rentOut?id=' + event.currentTarget.id,
        })
      }
    },
    // 加载更多
    uploadMore() {
      wx.requestSubscribeMessage({
        tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
        success: (res) => {
          if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
            wx.showToast({
              title: '订阅OK！',
              duration: 1000,
              success(data) {
                //成功
                // resolve()
                console.log(11111)
              }
            })
          }
        },
        fail(err) {
          //失败
          console.error(err);
          // reject()
        }
      })
      wx.navigateTo({
        url: '/pages/searchList/searchList?type=' + this.data.type,
      })
    },
    // 点赞
    like(event) {
      wxPromise.wxPost(API.USERRENTALLIKE, {
        id: event.currentTarget.id
      }, wx.getStorageSync('Token'))
      .then(res => {
        if (res.data.code == 1) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          toast.toast(res.data.msg);
        }
      })
    },
    // 踩
    dislike(event) {
      wxPromise.wxPost(API.USERRENTALDISLIKE, {
        id:event.currentTarget.id
      }, wx.getStorageSync('Token'))
      .then(res => {
        if (res.data.code == 1) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          toast.toast(res.data.msg);
        }
      })
    },
    // 收藏
    collect(event) {
      wxPromise.wxPost(API.USERRENTALCOLLECT, {
        id: event.currentTarget.id
      },
        wx.getStorageSync('Token'))
        .then(res => {
          if (res.data.code == 1) {
            wx.showToast({
              title: res.data.msg,
              icon: 'none'
            })
            this._callback();
          } else {
            toast.toast(res.data.msg);
          }
        })
    },
    // 定义一个回调函数
    _callback() {
      this.triggerEvent('collect');
    }
  }
})