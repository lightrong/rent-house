const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Component({
  properties:{
    haveStar:{
      type:Boolean,
      value:false
    },
    starCount:{
      type:String,
      value: '1'
    },
    labelList: {
      type: Array,
      value: []
    },
    // 评论id
    commentId:{
      type:Number,
      value:''
    },
    // 评论类型
    otype:{
      type:String,
      value:''
    },
    // 评论类型
    commentType:{
      type:String,
      value:'1'
    },
    // 举报id
    reportId: {
      type:String,
      value:''
    },
    focus: {
      type:Boolean,
      value:false
    }
  },
  data:{
    labelListIndex:[],
    textareaContent:'',
    checked:false
  },
  methods:{
    selectLabel(event){
      this.setData({
        labelListIndex: event.detail.value
      })
    },
    // 评分
    grade(event) {
      this.setData({
        starCount: event.target.dataset.star
      })
    },
    // 提交表单
    submitForm(event) {
      let flag = true;
      let warn = '';
      if(event.detail.value.content == '') {
        warn = '请填写内容'
      }
      else if(this.data.labelListIndex.length == 0){
        warn = '请选择标签'
      }
      else {
        flag = false;
        wx.showModal({
          title: '温馨提示',
          content: '确定提交？',
          success:result => {
            if(result.confirm){
              let tag = [];
              if (this.data.labelListIndex.length) {
                this.data.labelListIndex.forEach(value => {
                  tag.push(this.data.labelList[value]);
                })
              }
              if(this.data.commentId){
                const data = {
                  tag: tag.join('|'),
                  content: event.detail.value.content,
                  type: this.data.commentType,
                  otype: this.data.otype,
                  oid: this.data.commentId,
                  star: this.data.starCount
                }
                this.comment(data);
              }
              else if(this.data.reportId){
                const data = {
                  tag: tag.join('|'),
                  content: event.detail.value.content,
                  id:this.data.reportId
                }
                this.report(data);
              }
            }
          }
        })
      }
      if(flag) {
        wx.showToast({
          title: warn,
          icon:'none'
        })
      }
    },
    // 评论接口
    comment(data) {
      wx.showLoading({
        title: '',
      })
      wxPromise.wxPost(API.USERRENTALCOMMENT,data,wx.getStorageSync('Token'))
      .then(res => {
        wx.hideLoading();
        if(res.data.code == 1){
          wx.showToast({
            title: res.data.msg,
            icon:'none'
          })
          this._callback();
          this.setData({
            labelListIndex:[],
            textareaContent:'',
            checked:false
          })
        }
        else {
          toast.toast(res.data.msg);
        }    
      })
    },
    // 举报接口
    report(data) {
      wx.showLoading({
        title: '',
      })
      wxPromise.wxPost(
        API.USERRENTALREPORT,
        data,
        wx.getStorageSync('Token')
      ).then(res=> {
        wx.hideLoading();
        if(res.data.code == 1) {
          wx.showToast({
            title: res.data.msg,
            icon:'none',
            success:()=> {
              setTimeout(()=> {
                wx.navigateBack();
              },500)
            }
          })
        }else {
          toast.toast(res.data.msg);
        }
      })
    },
    // 定义一个回调函数
    _callback() {
      this.triggerEvent('comment');
    }
  }
})