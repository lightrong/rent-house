
Component({
  /**
   * 组件的属性列表
   */
  properties: {
  codeSuccess:String
  },

  /**
   * 组件的初始数据
   */
  data: {
    hiddenmodalput: true,
    testcode: '',
    textdetail: '',
    textvalue: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    inputChange: function (e) {
      this.setData({
        textdetail: e.detail.value
      })

    },
    // 生成验证码
    createCode: function () {
      var code = ""
      var codeLength = 6; //验证码的长度
      var random = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); //随机数
      for (var i = 0; i < codeLength; i++) { //循环操作
        var index = Math.floor(Math.random() * 36); //取得随机数的索引（0~35）
        code += random[index]; //根据索引取得随机数加到code上
      }
      this.setData({
        testcode: code
      })
      //把code值赋给验证码
    },
    //校验验证码
    validate: function () {
      var inputCode = this.data.textdetail.toUpperCase(); //取得输入的验证码并转化为大写      
      if (inputCode.length <= 0) { //若输入的验证码长度为0
        console.log("请输入验证码！"); //则弹出请输入验证码
      } else if (inputCode != this.data.testcode) { //若输入的验证码与产生的验证码不一致时
        wx.showToast({
          title: "验证码输入错误!",
          icon: "none"
        })
        console.log("验证码输入错误！@_@"); 
        //则弹出验证码输入错误
        this.createCode(); //刷新验证码
        this.setData({
          textvalue: ''
        }); //清空文本框
      } else { //输入正确时
        console.log('输入正确'); //弹出^-^
        this.hideModal();
        this.change();
      }
    },
    //点击按钮指定的hiddenmodalput弹出框 
    showDialogBtn: function () {
      this.setData({
        showModal: true
      })
      this.createCode()
     
      console.log(this.data.testcode)
    },
    /**
     * 弹出框蒙层截断touchmove事件
     */
    preventTouchMove: function () { },
    /**
     * 隐藏模态对话框
     */
    hideModal: function () {
      this.setData({
        showModal: false
      });
    },
    /**
     * 对话框取消按钮点击事件
     */
    onCancel: function () {
      this.hideModal();
    },
    /**
     * 对话框确认按钮点击事件
     */
    onConfirm: function () {
      this.validate()
    
    },
    change: function () {
      this.triggerEvent('myevent', { codeSuccess: 123 });
    }
  }
})