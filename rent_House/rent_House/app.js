//app.js
const API = require('utils/API.js');
const wxPromise = require('utils/promise.js');
const getLocation = wxPromise.wxPromise(wx.getLocation);
// 引入SDK核心类
const QQMapWX = require('libs/qqmap-wx-jssdk1.0/qqmap-wx-jssdk.min.js');
const qqmapsdk = new QQMapWX({
  "key": 'K6BBZ-SSW3G-LFFQQ-I2C5P-GA42O-GVFAT'
});
App({
  onLaunch: function () {
    // 登录
    wx.login({
      success: res => {
        wxPromise.wxPost(API.USERPUBLICGETOPENID,{
          code:res.code
        })
        .then(res=> {
          if(res.data.code == 1){
            wx.setStorageSync('openid', res.data.data.openid);
          }
        })
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  onHide() {
    if(!this.globalData.formId.length || !wx.getStorageSync('Token')) {
      return
    }
    let arr = [...new Set(this.globalData.formId)]
    wxPromise.wxPost(API.USERRENTALSETFORMID, {
      form_id: arr.join('|')
    }, wx.getStorageSync('Token'))
    .then(res => {
      this.globalData.formId = []
    })
    .catch(err=> {
      
    })
  },
  // 获取地理位置
  getLocation(obj) {
    getLocation()
    .then(result => {
      // 你地址解析
      qqmapsdk.reverseGeocoder({
        location: {
          latitude: result.latitude,
          longitude: result.longitude
        },
        success: area => {
          this.globalData.currentCity = area.result.address_component.city;
          this.globalData.region = [area.result.address_component.province, area.result.address_component.city, area.result.address_component.district];
          obj.setData({
            currentArea: area.result.address_component.city,
            district: area.result.address_component.district,
            region: [area.result.address_component.province, area.result.address_component.city, area.result.address_component.district]
          })
          // 获取主页信息
          obj.getHomeIndex(area.result.address_component.city);
        },
        fail: area => {
          // 获取主页信息
          obj.getHomeIndex();
        }
      });
    })
  },
  globalData: {
    userInfo: null,
    currentCity:null,
    region:[],
    formId:[]
  }
})