//封装get请求方法
function wxGet(url,data = {},token = '') {
  wx.showNavigationBarLoading();
  return new Promise((resove, reject) => {
    wx.request({
      url: url,
      data: data,
      header: {
        "XX-Device-Type":"wxapp",
        "XX-Token":token,
        'content-type': 'application/json'
      },
      method: 'GET', 
      success: res=> {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        resove(res);
      },
      fail: msg=> {
        console.log('reqest error', msg);
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        reject('fail');
      }
    })
  })
}

// 封装post请求
function wxPost(url,data = {},token = '') {
  wx.showNavigationBarLoading();
  return new Promise((resove, reject) => {
    wx.request({
      url: url,
      data: data,
      header: {
        "XX-Device-Type":"wxapp",
        "XX-Token":token,
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST', 
      success: res=> {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        resove(res);
      },
      fail: msg=> {
        console.log('reqest error', msg);
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        reject('fail');
      }
    })
  })
}

// 公共的promise
function wxPromise(fn){
  return function(obj={}){
    return new Promise((resove,reject)=> {
      obj.success = res => resove(res);
      obj.fail = res => reject(res);
      fn(obj);
    })
  }
}

// 模块导出
module.exports = {
  wxGet,
  wxPost,
  wxPromise
}