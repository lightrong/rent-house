// 时间格式化
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')

}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const thumbTime = date => {
  // 当前时间
  let timestamp = new Date();
  const newYear = timestamp.getFullYear()
  const newMonth = timestamp.getMonth() + 1
  const newDay = timestamp.getDate()
  const newHour = timestamp.getHours()
  const newMinute = timestamp.getMinutes()
  const newSecond = timestamp.getSeconds()
  // 发布时间
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  if (newYear - year != 0) {
    return newYear - year + '年前'
  }
  else if (newMonth - month != 0) {
    return newMonth - month + '个月前'
  }
  else if (newDay - day != 0) {
    return newDay - day + '天前'
  }
  else if (newHour - hour != 0) {
    return newHour - hour + '小时前'
  }
  else if (newMinute - minute != 0) {
    return newMinute - minute + '分钟前'
  }
  else {
    return '刚刚'
  }
}



// 暴露接口
module.exports = {
  formatTime,
  thumbTime
}
