// 公共url前缀
 const HOST = 'https://rental.homhere.cn/api/';
 const COMMON = "https://rental.homhere.cn/"
//  const HOST = 'https://dev.homhere.cn/api/';
//  const COMMON = "https://dev.homhere.cn/"

// 接口导出
module.exports = {
  // 公共url前缀
  COMMON,
  // 主页
  HOMEINDEX:`${HOST}home/Index`,
  // 个人中心出租求租列表
  APIUSERPERSONALRENTING_LIST:`${HOST}user/personal/renting_list`,
  // 个人中心出租，求组详情
  APIHOMERENTALRENTING_DETAILS:`${HOST}home/rental/renting_details`,
  // 个人中心出租，求组列表(专供其他用户查看，无需登录)
  APIHOMERENTALRENTING_LIST:`${HOST}home/rental/renting_list`,
  // [需登录]修改文章
  APIUSERPERSONALARTICLE_EDIT:`${HOST}user/personal/article_edit`,
  // [需登录]删除文章
  APIUSERPERSONALARTICLE_DELETE:`${HOST}user/personal/article_delete`,
  // [需登录] 发布出租
  USERRENTALLEASE:`${HOST}user/rental/lease`,
  // [需登录] 小程序端发起支付： 需要付费发布时调用
  USERRENTALCREATEPAYDATAFORMINIAPP:`${HOST}user/rental/createPayDataforMiniApp`,
  // [需登录] 发布求租
  USERRENTALRENT:`${HOST}user/rental/rent`,
  // 搜索房源
  HOMESEARCHINDEX:`${HOST}home/search/index`,
  // 查看联系方式
  USERPUBLICVIEWCONTACT:`${HOST}user/public/viewContact`,
  // [需登录] 点赞，顶
  USERRENTALLIKE:`${HOST}user/rental/like`,
  // [需登录] 踩
  USERRENTALDISLIKE:`${HOST}user/rental/dislike`,
  // [需登录] 分享
  USERRENTALSHARE:`${HOST}user/rental/share`,
  // [需登录] 收藏
  USERRENTALCOLLECT:`${HOST}user/rental/collect`,
  // [需登录] 举报
  USERRENTALREPORT:`${HOST}user/rental/report`,
  // [需登录] 评论、回复
  USERRENTALCOMMENT:`${HOST}user/rental/comment`,
  // [需登录] 修改出租
  USERRENTALEDITLEASE:`${HOST}user/rental/editLease`,
  // [需登录] 修改求租
  USERRENTALEDITRENT:`${HOST}user/rental/editRent`,
  // [需登录] 删除房源
  USERRENTALDELRENT:`${HOST}user/rental/delRent`,
  //获取房源列表
  USERRENTALGETRENTALLIST: `${HOST}user/rental/getRentalList`,
  // 发送短信（注册、找回密码使用）
  USERVERIFICATION_CODESEND:`${HOST}user/verification_code/send`,
  // 注册
  USERPUBLICREGISTER:`${HOST}user/public/register`,
  // 登录
  USERPUBLICLOGIN:`${HOST}user/public/login`,
  // [需登录] 退出
  USERPUBLICLOGOUT:`${HOST}user/public/logout`,
  // 重置密码
  USERPUBLICPASSWORDRESET:`${HOST}user/public/passwordreset`,
  // 获取配置参数
  USERPUBLICGETCONFIG:`${HOST}user/public/getconfig`,
  // 根据城市获取区列表
  HOMESEARCHGETAREA :`${HOST}home/search/getArea`,
  // 获取opening
  USERPUBLICGETOPENID:`${HOST}user/Public/getOpenid`,
  // 查看个人简介接口
  HOMERENTALINDEX:`${HOST}home/rental/index`,
  // [需登录]修改个人简介信息
  USERPERSONALEDIT_USER:`${HOST}user/personal/edit_user`,
  // [需登录]获取个人基本信息
  USERPERSONALINDEX:`${HOST}user/personal/index`,
  // [需登录]添加/修改文章
  USERPERSONALARTICLE_POST:`${HOST}user/personal/article_post`,
  // [需登录]个人中心文章管理
  USERPERSONALARTICLE_LIST:`${HOST}user/personal/article_list`,
  // [需登录]个人中心评论管理
  USERPERSONALCOMMENT_MANAGE:`${HOST}user/personal/comment_manage`,
  // [需登录]个人中心收藏列表
  USERPERSONALCOLLECTION:`${HOST}user/personal/collection`,
  // [需登录]个人中心我的举报
  USERPERSONALREPORT:`${HOST}user/personal/report`,
  // 文章详情
  HOMERENTALARTICLE_DETAILS:`${HOST}home/rental/article_details`,
  // 文章列表（无需登录）
  HOMERENTALARTICLE_LIST:`${HOST}home/rental/article_list`,
  // 上传单张图片
  USERRENTALUPLOADIMG:`${HOST}user/rental/uploadImg`,
  // 	发布出租
  USERRENTALWXAPPLEASE:`${HOST}user/rental/wxapplease`,
  // 检查是否能发布
  USERRENTALCHECKRENTAL: `${HOST}user/rental/checkRental`,
  // 修改出租
  USERRENTALEDITWXAPPLEASE:`${HOST}user/rental/editWxapplease`,
  // 修改房源状态
  USERRENTALSTATUSRENT:`${HOST}user/rental/statusRent`,
  //判断是否能发消息
  USERRENTALCHECKMESSAGE: `${HOST}user/rental/checkMessage`,
  // [需登录]添加/修改文章
  USERPERSONALARTICLE_POST_WXAPP: `${HOST}user/personal/article_post_wxapp`,
  // [需登录]个人中心房源评论管理
  USERPERSONALRENTAL_COMMENT_MANAGE:`${HOST}user/personal/rental_comment_manage`,
  // [需登录]删除评论
  USERRENTALDELETE_COMMENT:`${HOST}user/rental/delete_comment`,
  // 生成海报
  HOMERENTALCREATEPOESTER: `${HOST}home/rental/createPoester`,
  // 获取聊天列表(聊天功能)
  USERRENTALGETMESSAGE: `${HOST}/user/rental/getMessage`,
  // 获取聊天详情信息（聊天功能）
  USERRENTALMESSAGEDETAILS: `${HOST}/user/rental/messageDetails`,
  // 发送信息（聊天功能）
  USERRENTALSENDMESSAGE: `${HOST}/user/rental/send_message`,
  // 开启/关闭系统通知
  USERPERSONALEDITNOTICE: `${HOST}/user/personal/editNotice`,
  // 发送formid
  USERRENTALSETFORMID:`${HOST}/user/rental/setFormId`,
  HOMERENTALIFRENTALSTATUS: `${HOST}home/rental/ifRentalStatus`,
  //房源下架
  USERRENTALOUTRENT: `${HOST}/user/rental/outRent`,
  //获取有新消息
   USERRENTALGETMESSAGESTATUS: `${HOST}/user/rental/getMessageStatus`,
}
