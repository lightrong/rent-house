const toast  = (msg)=> {
  wx.showToast({
    title: msg,
    icon: 'none',
    success: () => {
      setTimeout(() => {
        wx.navigateTo({
          url:'/selfer/login/login'
        })
      }, 500)
    }
  })
}
const toast2 = res=>{
  wx.showToast({
    title: res.data.msg,
    icon: 'none',
  })
}
module.exports = {
  toast,
  toast2
}