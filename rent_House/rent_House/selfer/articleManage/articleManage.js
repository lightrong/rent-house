// selfer/articleManage/articleManage.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    articleList:[],
    showLoading:true
  },
  // 获取文章列表
  getArticleList() {
    wxPromise.wxPost(
      API.USERPERSONALARTICLE_LIST,
      {page},
      wx.getStorageSync('Token')
    )
    .then(res=> {
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1) {
        this.setData({
          articleList:[...this.data.articleList,...res.data.data.data]
        })
      }
      else {
        toast.toast(res.data.msg);
      }
    })
  },
  // 删除文章
  deleteArticle (event) {
    wx.showModal({
      title: '温馨提示',
      content: '确认删除？',
      success:result=>{
        if(result.confirm){
          wxPromise.wxPost(API.APIUSERPERSONALARTICLE_DELETE,{
            id:event.currentTarget.id
          },wx.getStorageSync('Token'))
          .then(res => {
            if(res.data.code == 10001) {
              toast.toast(res.data.msg);
            }
            else if(res.data.code ==1) {
              this.setData({
                articleList:[],
                showLoading:true
              })
              this.getArticleList()
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
            }
            else {
              wx.showToast({
                title: res.data.msg,
                icon:'none'
              })
            }
          })
        }
      }
    })
  },
  // 跳转编辑文章
  goToEditArticle(event) {
    // wx.navigateTo({
    //   url: '../../issue/issueArticle/issueArticle?id=' + event.currentTarget.id,
    // })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    page = 1;
    this.getArticleList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    page = 1;
    this.setData({
      articleList:[]
    })
    this.getArticleList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page++;
    this.getArticleList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})