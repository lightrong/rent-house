// 发布求租
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkInTime:'',
    addressData:[],
    houseType: 0,
    facility: [],
    facilityIndex: [],
    phoneAndWechat: false,
    homeTitle:'',
    id:'',
    index:'',
    status:'',
    mobile:'',
    isOpen: 2
  },
  //是否公开联系方式复选框
  isOpen: function (res) {
    if (this.data.isOpen == 2) {
      this.setData({
        isOpen: 1
      });
    } else {
      this.setData({
        isOpen: 2
      });
    }
  },
  // 单选框出租类型
  radioChange(event) {
    this.setData({
      houseType: event.detail.value
    })
  },
  // 选择时间
  selectTime(event) {
    this.setData({
      checkInTime:event.detail.value
    })
  },
  // 选择地区
  selectAddress(event) {
    this.setData({
      addressData:event.detail.value
    })
  },
  // 设备选择
  selectFacility(event) {
    this.setData({
      facilityIndex: event.detail.value
    })
  },
  // 手机号与微信相同
  phoneAndWechat(event) {
    if (!event.detail.value.length) {
      this.setData({
        phoneAndWechat: 0
      })
    }
    else {
      this.setData({
        phoneAndWechat: 1
      })
    }
  },
  // 选择房源
  selectStatus(event) {
    this.setData({
      status: parseInt(event.detail.value, 10)
    })
  },
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      this.setData({
        facility: res.data.data.furniture,
      })
    })
  },
  //  表单提交
  submitForm(event) {
    let flag = true;
    let formData = event.detail.value
    let warn = '';
    if (formData.houseType == '') {
      warn = '请选择房源类型'
    }
    else if (formData.title == '') {
      warn = '请填写标题'
    }
    else if (formData.time == '') {
      warn = '请选择入住时间'
    }
    else if (formData.region.length == 0) {
      warn = '请选择地址'
    }
    else if (formData.description == '') {
      warn = '请填写房屋描述'
    }
    else if (formData.monthly == '') {
      warn = '请填写房租'
    }
    else if (formData.mobile == '') {
      warn = '请填写手机号码'
    }
    else if (formData.facility.length == 0) {
      warn = '请选择设备'
    }
    else {
      flag = false;
      wx.showModal({
        title: '温馨提示',
        content: '确定修改？',
        success:result => {
          if(result.confirm) {
            // let facility = [];
            // formData.facility.forEach(value => {
            //   facility.push(this.data.facility[value].name);
            // })
            let data = {
              is_open_contact: this.data.isOpen,
              id:this.data.id,
              release_status: formData.status,
              source: parseFloat(formData.houseType),
              title: formData.title,
              prov: formData.region[0],
              city: formData.region[1],
              area: formData.region[2],
              desc: formData.description,
              monthly: parseFloat(formData.monthly),
              furniture: formData.facility.join("|"),
              mobile: formData.mobile,
              check_in_time:formData.time,
              is_wechat_same_mobile: this.data.phoneAndWechat
            }
            this.editDemandRent(data);
          }
        }
      })
    }
    if(flag){
      wx.showToast({
        title: warn,
        icon:'none'
      })
    }
  },


  //删除出租接口
  deleteHouse() {
    wx.showModal({
      title: '温馨提示',
      content: '确定删除？',
      success: result => {
        if (result.confirm) {
          wx.showLoading({
            title: '',
          })
          wxPromise.wxPost(API.USERRENTALDELRENT, {
            id: this.data.id
          }, wx.getStorageSync('Token'))
            .then(res => {
              wx.hideLoading();
              if (res.data.code == 1) {
                wx.showToast({
                  title: res.data.msg,
                  icon: 'none',
                  success:()=>{
                    setTimeout(()=>{
                      wx.navigateBack({
                        
                      })
                    },500)
                  }
                })
              }
              else if (res.data.code == 10001) {
                toast.toast(res.data.msg);
              }
              else {
                wx.showToast({
                  title: res.data.msg,
                  icon: 'none'
                })
              }
            })
        }
      }
    })
  },
  // 修改求租
  editDemandRent(data) {
    wx.showLoading({
      title: '',
    })
    wxPromise.wxPost(
      API.USERRENTALEDITRENT,
      data,
      wx.getStorageSync('Token')
    ).then(res=> {
      wx.hideLoading();
      if(res.data.code ==10001) {
        toast.toast(res.data.msg);
      }
      else if(res.data.code == 1){
        wx.showToast({
          title: res.data.msg,
          icon:'none',
          success:()=> {
            setTimeout(()=> {
              wx.navigateBack();
              const pages = getCurrentPages();
              const prePage = pages[pages.length - 2];
              prePage.setData({
                houseList:[],
                showLoading:true
              })
              prePage.getHouseList();
            },500)
          }
        })
      }else {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
    })
  },
  // 取消按钮
  cancel() {
    wx.navigateBack();
  },
  // 获取详情的数据
  getHomeDetail(id) {
    wxPromise.wxPost(API.APIHOMERENTALRENTING_DETAILS, { id })
      .then(res => {
        let furnitureIndex = [];
        res.data.data.list.furniture.forEach(value => {
          furnitureIndex.push(value.name);
        })
        let type_str = '';
        if (res.data.data.list.type_str == '个人求租') {
          type_str = 1;
        } else {
          type_str = 2
        }
        this.setData({
          showLoading: false,
          homeDetail: res.data.data.list,
          facilityIndex: furnitureIndex,
          houseType: type_str,
          checkInTime:res.data.data.list.check_in_time.split(' ')[0],
          addressData: [res.data.data.list.prov, res.data.data.list.city, res.data.data.list.area],
          status: res.data.data.list.release_status,
          mobile: res.data.data.list.mobile,
        })
        this.getContact();
      })
  },
  // 获取上一页信息 
  getPrePage() {
    const pages = getCurrentPages();
    const prePage = pages[pages.length - 2];
    this.setData({
      homeTitle: prePage.data.houseList[this.data.index].title,
      audit_status: prePage.data.houseList[this.data.index].audit_status
    })
  },
  // 获取电话号码
  getContact() {
    wxPromise.wxPost(API.USERPUBLICVIEWCONTACT, {
      id: this.data.id
    }, wx.getStorageSync('Token'))
      .then(res => {
        console.log(res);
        if (res.data.code == 1) {
          this.setData({
            mobile: res.data.data.mobile,
            phoneAndWechat: parseInt(res.data.data.is_wechat_same_mobile, 10)
          })
        } else if (res.data.code == 10001) {
          toast.toast(res.data.msg);
        }
        else {

        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      host:API.COMMON,
      id:options.id,
      index:options.index
    })
    this.getconfig();
    this.getHomeDetail(options.id);
    this.getPrePage();
    this.getContact();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})