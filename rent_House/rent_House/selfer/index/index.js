const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({
  // 初始化数据
  data: {
    showPage:false,
    token:'',
    info:'',
    isChecked:true,
    isAuth:false
  },
  // 通知开关
  changeSwitch:function(res){
    wxPromise.wxPost(API.USERPERSONALEDITNOTICE, {}, wx.getStorageSync('Token')).then(res=>{
      wx.showToast({
        title: res.data.msg,
        icon: 'none'
      })
    })
    this.setData({
      isChecked: res.detail.value
    })
  },
  // 退出登录 
  logout() {
    wx.showModal({
      title: '温馨提示',
      content: '确定退出当前账号？',
      success:data=> {
        if(data.confirm){
          wxPromise.wxPost(API.USERPUBLICLOGOUT, {}, wx.getStorageSync('Token'))
          .then(res => {
            let warn = '';
            if (res.data.code == 1) {
              wx.showToast({
                title: res.data.msg,
                icon: 'none',
                success: () => {
                  try {
                    wx.setStorageSync('Token','');
                  } 
                  catch (e) {
                    throw new Error(e);
                  }
                  this.getUserInfo();
                }
              })
            }
            else {
              toast.toast(res.data.msg);
            }
          })
        }
      }
    })
  },
  // 
  // 是否跳转
  canTap() {
    if (!wx.getStorageSync('Token')) {
      wx.showToast({
        title: '请先登录',
        icon:'none'
      })
    }
  },
  // 获取用户信息
  getUserInfo() {
    wxPromise.wxGet(API.USERPERSONALINDEX, {}, wx.getStorageSync('Token'))
    .then(res => {
      this.setData({
        showLoading: false
      })
      wx.setStorageSync("id", res.data.data.id)
      if (res.data.code == 1) {
        this.setData({
          info: res.data.data,
        })
        // if (this.data.info.user_nickname){
        //   wx.setStorageSync(myName, this.data.info.user_nickname)
        // }
      } 
      else if(res.data.code == 10001){
        wx.setStorageSync('Token', '');
        this.setData({
          info:'',
          token: wx.getStorageSync('Token')
        })  
      }
    })
  },
  // 获取微信头像和昵称
  getWXInfo(res) {
    if (res.detail.errMsg == "getUserInfo:fail auth deny") {
      return;
    }
    wx.navigateTo({
      url: '../login/login',
    })
  },
  // 展示图片 
  showImage() {
    wx.previewImage({
      current: this.data.host + this.data.info.avatar,
      urls: [this.data.host + this.data.info.avatar]
    })
  },

  // 监听页面加载
  onLoad(options){
    wx.requestSubscribeMessage({
      tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
      success: (res) => {
        if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
          wx.showToast({
            title: '订阅OK！',
            duration: 1000,
            success(data) {
              //成功
              // resolve()
              console.log(11111)
            }
          })
        }
      },
      fail(err) {
        //失败
        console.error(err);
        // reject()
      }
    })
    this.setData({
      host : API.COMMON
    })
    this.getUserInfo();
  },
  // 监听页面显示
  onShow() {
    this.setData({
      token: wx.getStorageSync('Token')
    })
    // 查看是否授权
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          this.setData({
            isAuth: true
          })
        }
      }
    })
    if(this.data.showPage) {
      this.getUserInfo()      
    }
  },
  // 监听页面隐藏
  onHide() {
    this.setData({
      showPage:true
    })
  },
  onShareAppMessage() {

  }
})