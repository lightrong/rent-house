// 个人中心
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    genderData:['保密','男','女'],
    genderIndex:'',
    imageUrl: [],
    showLoading:true
  },
  // 选择性别
  selectGender(event) {
    this.setData({
      genderIndex:event.detail.value
    })
  },
  // 修改头像
  editIcon() {
    this.uploadingImage();
  },
  // 上传图片
  uploadingImage() {
    // 选择相片
    wx.chooseImage({
      count: 1,
      success: res => {
        this.setData({
          imageUrl: res.tempFilePaths,
          maxCount: 1
        });
      },
    })
  },
  // 获取用户信息
  getUserInfo() {
    wxPromise.wxGet(API.USERPERSONALINDEX,{},wx.getStorageSync('Token'))
    .then(res=> {
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1){
        this.setData({
          info:res.data.data,
          genderIndex:res.data.data.sex
        })
      }else {
        toast.toast(res.data.msg);
      }
    })
  },
  // 上传头像
  uploadHeaderImg(path,data) {
    wx.showLoading({
      title: '',
    })
    if(path){
      wx.uploadFile({
        url: API.USERPERSONALEDIT_USER,
        filePath: path,
        header: {
          "XX-Device-Type": "wxapp",
          "XX-Token": wx.getStorageSync('Token'),
          'content-type': 'multipart/form-data'
        },
        name: 'avatar',
        formData: data,
        success:res=> {
          wx.hideLoading();
          if(JSON.parse(res.data).code ==1) {
            wx.showToast({
              title: JSON.parse(res.data).msg,
              icon:'none',
              success: () => {
                setTimeout(()=> {
                  wx.navigateBack();
                },500)
              }
            })
          }
          else if(JSON.parse(res.data).code == 10001) {
            toast.toast(JSON.parse(res.data).msg);
          }
          else {
            wx.showToast({
              title: JSON.parse(res.data).msg,
              icon:'none'
            })
          }
        }
      })
    }
    else {
      wxPromise.wxPost(API.USERPERSONALEDIT_USER, data, wx.getStorageSync('Token'))
      .then(res=> {
        wx.hideLoading();
        if (res.data.code == 1) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            success:() => {
              setTimeout(() => {
                wx.navigateBack();
              }, 500)
            }
          })
        }
        else if (res.data.code == 10001) {
          toast.toast(res.data.msg);
        }
        else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        }
      })
    }
  },
  // 保存按钮
  submitForm(event) {
    let flag = true;
    let warn = ''
    if (event.detail.value.user_nickname == ''){
      warn = "请填写昵称";
    }
    else {
      wx.showModal({
        title: '温馨提示',
        content: '确定保存修改',
        success:result => {
          if(result.confirm) {
            this.uploadHeaderImg(this.data.imageUrl[0],event.detail.value);
          }
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      host: 'https://rental.homhere.cn'
    })
    this.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})