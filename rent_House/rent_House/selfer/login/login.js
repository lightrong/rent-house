const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const showToast = wxPromise.wxPromise(wx.showToast);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rememberStatus:0,
    password:'',
    phoneNumber:'',
    jumpMy:false,
    path:'',
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isAuth:false
  },
  // 记住密码？
  checkboxChange(event){
    if(this.data.rememberStatus == 0) {
      this.setData({
        rememberStatus: 1,
      })
    }else {
      this.setData({
        rememberStatus: 0,
      })
    }
  },
  // 跳转注册页面
  getRegister(){
    wx.redirectTo({
      url: '../register/register',
    })
  },
  // 跳转忘记密码
  forgetPassword() {
    wx.redirectTo({
      url: '../forgetPassword/forgetPassword',
    })
  },
  // 登录按钮
  formSubmit(event) {
    const data = {
      user_login:event.detail.value.phoneNumber,
      user_pass:event.detail.value.password,
      device_type:'wxapp',
      openid: wx.getStorageSync('openid')
    }
    // 请求登录
    this.login(data);
  },
  // 发送登录请求
  login(data) {
    wx.showLoading({
      title: '正在登录',
    })
    wxPromise.wxPost(API.USERPUBLICLOGIN,data)
    .then(res=> {
      wx.hideLoading();
      if(res.data.code == 1){
        wx.setStorageSync('Token', res.data.data.token);
        wx.setStorageSync('phone', data.user_login);
        // wx.setStorageSync('phone', data.user_login);
        if (this.data.rememberStatus == 1){
          wx.setStorageSync('password', data.user_pass);
        }
        else {
          try {
            wx.removeStorageSync('password');
          } catch (e) {
            throw new Error(e);
          }
        }
        // this.setData({
        //   jumpMy:true
        // })
        // 跳转我的页面
        showToast({
          title: res.data.msg,
          icon:'none',
        })
        .then(result => {
          setTimeout(() => {
            // wx.switchTab({
            //   url: this.data.path,
            // })
            // wx.navigateBack();
            var pages = getCurrentPages();//当前页面栈
            if (pages.length > 1) {
              var beforePage = pages[pages.length - 2];//获取上一个页面实例对象
              var currPage = pages[pages.length - 1]; // 当前页面，若不对当前页面进行操作，可省去
              beforePage.setData({       //如果需要传参，可直接修改A页面的数据，若不需要，则可省去这一步
                isLogin: wx.getStorageSync('Token') ? true : false
              })
              try{
                beforePage.changeData();//触发父页面中的方法
              }catch(err){

              }
            }
            wx.navigateBack({
              delta: 1
            })
          }, 500)
        })
      }
      else {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
    })
  },
  // 获取微信头像和昵称
  getWXInfo(res) {
    if (res.detail.errMsg == "getUserInfo:fail auth deny") {
      return;
    }
    wx.redirectTo({
      url: '../register/register',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(wx.getStorageSync('password')){
      this.setData({
        rememberStatus:1
      })
    }
    else {
      this.setData({
        rememberStatus:0
      })
    }
    this.setData({
      password:wx.getStorageSync('password'),
      phoneNumber:wx.getStorageSync('phone')
    })
    if(options.path){
      this.setData({
        path : options.path
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          this.setData({
            isAuth: true
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    // if(!this.data.jumpMy){
    //   if(this.data.path){
    //     wx.navigateBack()
    //   }else {
    //     wx.switchTab({
    //       url: '../../pages/index/index',
    //     })
    //   }
    // }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})