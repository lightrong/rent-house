// 个人出租
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    houseList:[],
    showLoading:true
  },

  // 获取房源列表
  getHouseList (uploadType) {
    wxPromise.wxPost(
      API.APIUSERPERSONALRENTING_LIST,
      {
        type:1,
        page,
      },
      wx.getStorageSync('Token')
    ).then(res=> {
      let data = res.data.data.data;
      data.forEach((value,index)=> {
        value.monthly = parseInt(value.monthly).toFixed(0);
      })
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1) {
        if(uploadType){
          this.setData({
            houseList: data
          })
        }else {
          this.setData({
            houseList : [...this.data.houseList,...data]
          })
        }
      }else {
        toast.toast(res.data.msg)
      }
    })
  },
  // 获取房源详情
  getHouseDetail(event) {
    wx.navigateTo({
      url: '../../pages/rentOut/rentOut?id=' + event.currentTarget.id,
    })
  },
  // 获取修改页面
  editHouse(event) {
    wx.navigateTo({
      url: '../editRentOut/editRentOut?id=' + event.currentTarget.id + '&index=' + event.currentTarget.dataset.index,
    })
  },

  //  修改房源状态
  editHouseStatus(event) {
    if (parseInt(event.currentTarget.dataset.status, 10) == 2) return;
    let release_status = '', release_name = '';
    if (parseInt(event.currentTarget.dataset.status, 10) == 1) {
      release_status = 2;
      release_name = '已出租'
    } else {
      release_status = 1;
      release_name = '待出租'
    }
    const data = {
      id: event.currentTarget.id,
      release_status
    }
    wx.showModal({
      title: '温馨提示',
      content: '确定修改房源状态为' + release_name + '？',
      success: res => {
        if (res.confirm) {
          wxPromise.wxPost(
            API.USERRENTALSTATUSRENT,
            data,
            wx.getStorageSync('Token')
          ).then(res => {
            if (res.data.code == 1) {
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
              this.getHouseList(1);
            }
            else if (res.data.code == 10001) {
              toast.toast(res.data.msg);
            }
            else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
            }
          })
        }
      }
    })
  },

  //下架房源

  out:function(e){
    var that=this
    const data = {
      id: e.currentTarget.dataset.id,
    }
    wxPromise.wxPost(
      API.USERRENTALOUTRENT,
      data,
      wx.getStorageSync('Token')
    ).then(res=>{
      if(res.data.code==1){
          // console.log(res)
        that.onPullDownRefresh()
      }else{
        wx.showToast({
          title: res.data.msg,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    page = 1;
    this.getHouseList();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      houseList:[],
      showLoading: true
    })
    page = 1;
    this.getHouseList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page++;
    this.getHouseList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})