// selfer/myCollection/myCollection.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    house_resource: [
    ],
    collectionList:[],
    showLoading:true
  },
  // 获取收藏列表
  getCollectionList() {
    wxPromise.wxGet(API.USERPERSONALCOLLECTION,{page},wx.getStorageSync('Token'))
    .then(res => {
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1) {
        this.setData({
          collectionList: [...this.data.collectionList,...res.data.data.data]
        })
      }
      else {
        toast.toast(res.data.msg);
      }
    })
  },
  // 组件的一个回调
  _collect() {
    page =1 ;
    this.setData({
      collectionList:[]
    })
    this.getCollectionList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    page = 1;
    this.getCollectionList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    page = 1;
    this.setData({
      showLoading: true,
      collectionList:[]
    })
    this.getCollectionList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page ++ ;
    this.getCollectionList();    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})