// selfer/commentManage/commentManage.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
let page = 1,housePage = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    commentList:[],
    houseCommentList:[],
    haveComment:true,
    total:'',
    houseTotal:'',
    downLoading:false,
    completeLoading:false,
    showLoading:true,
    type:1
  },
  //  动画函数
  selectType(event) {
    this.animation.translateX(event.currentTarget.dataset.distance).step();
    this.setData({
      animationData: this.animation.export(),
      type: event.currentTarget.dataset.type
    })
    if(this.data.type == 1) {
      page = 1
      this.getCommentList((res) => {
        this.setData({
          commentList: res.data.data.data,
          total: res.data.data.total
        })
      })
    }
    else {
      housePage = 1;
      this.getHouseCommentList((res) => {
        this.setData({
          houseCommentList: res.data.data.data,
          houseTotal: res.data.data.total
        })
      });
    }
  },
  // 获取用户评论列表
  getCommentList(fn) {
    wxPromise.wxPost(
      API.USERPERSONALCOMMENT_MANAGE,
      {page},
      wx.getStorageSync('Token')
    ).then(res=> {
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1) {
        if(typeof fn == 'function') {
          fn(res);
        }
      }
      else if(res.data.code == 10001) {
        toast.toast(res.data.msg);
      }
      else {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
    })
  },
  // 获取房源评论列表
  getHouseCommentList(fn) {
    wxPromise.wxPost(
      API.USERPERSONALRENTAL_COMMENT_MANAGE,
      {page:housePage},
      wx.getStorageSync('Token')
    ).then(res=> {
      this.setData({
        showLoading: false
      })
      if (res.data.code == 1) {
        if (typeof fn == 'function') {
          fn(res);
        }
      }
      else if (res.data.code == 10001) {
        toast.toast(res.data.msg);
      }
      else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },
  // 一个回调函数
  _comment () {
    page = 1;
    housePage = 1;
    if(this.data.type == 1){
      this.getCommentList((res)=> {
        this.setData({
          commentList: res.data.data.data
        })
      });
    }
    else {
      this.getHouseCommentList((res) => {
        this.setData({
          houseCommentList: res.data.data.data,
        })
      });
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    page = 1;
    this.getCommentList((res)=> {
      this.setData({
        commentList: res.data.data.data,
        total:res.data.data.total
      })
    });
    this.animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease-out',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // page =1;
    // housePage = 1;
    // this.setData({
    //   showLoading: true
    // })
    // if(this.data.type == 1){
    //   this.getCommentList((res) => {
    //     this.setData({
    //       commentList: res.data.data.data,
    //       type:1
    //     })
    //   });
    // }
    // else {
    //   this.getHouseCommentList((res) => {
    //     this.setData({
    //       commentList: res.data.data.data,
    //       type:2
    //     })
    //   });
    // }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      downLoading: true,
      completeLoading:false
    })
    if(this.data.type == 1) {
      page++;
      this.getCommentList(res=> {
        if(res.data.data.data.length == 0) {
          this.setData({
            completeLoading:true
          })
        }
        this.setData({
          commentList: [...this.data.commentList, ...res.data.data.data],
          downLoading:false
        })
      })
    }
    else {
      housePage++;
      this.getHouseCommentList((res) => {
        if(res.data.data.data.length == 0) {
          this.setData({
            completeLoading:true
          })
        }
        this.setData({
          houseCommentList: [...this.data.houseCommentList, ...res.data.data.data],
          downLoading:false
        })
      });
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})