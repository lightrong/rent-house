const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const uploadFile = wxPromise.wxPromise(wx.uploadFile);
const getSetting = wxPromise.wxPromise(wx.getSetting);
const getLocation = wxPromise.wxPromise(wx.getLocation);
const authorize = wxPromise.wxPromise(wx.authorize);
// 引入SDK核心类
const QQMapWX = require('../../libs/qqmap-wx-jssdk1.0/qqmap-wx-jssdk.min.js');
const qqmapsdk = new QQMapWX({
  "key": 'K6BBZ-SSW3G-LFFQQ-I2C5P-GA42O-GVFAT'
});
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageUrlList:[],
    showAdd:true,
    maxCount:20,
    houseType:0,
    houseLabel:[],
    array:[],
    rentType:'',
    region:['北京市','北京市','朝阳区'],
    typeIndex:[0,0],
    subway:[],
    subwayIndex:0,
    facility: {},
    facilityIndex:[],
    phoneAndWechat:0,
    limit_tag: [],
    homeDetail:{},
    id:'',
    index:'',
    mobile:'',
    status:'',
    showLoading:true,
    check_in_time:'',
    isOpen:2
  },
  isOpen:function(res){
    if(this.data.isOpen==2){
      this.setData({
        isOpen:1
      });
    }else{
      this.setData({
        isOpen: 2
      });
    }
  },
  // 图片预览
  preview(event){
    wx.previewImage({
      current:event.currentTarget.dataset.image,
      urls: this.data.imageUrlList,
    })
  },
  // 上传图片
  uploadingImage (){
    const that = this;
    // 设置最大的上传张数
    that.setData({
      maxCount:that.data.maxCount - that.data.imageUrlList.length
    });
    // 选择相片
    wx.chooseImage({
      count: that.data.maxCount,
      success: res=> {
        that.setData({
          imageUrlList: [...that.data.imageUrlList, ...res.tempFilePaths],
          maxCount:20
        });
        // 判断是否显示上传按钮
        if (that.data.imageUrlList.length >= this.data.maxCount) {
          that.setData({
            showAdd:false
          })
        }
      },
    })
  },
  // 删除按钮
  deleteImg(event){
    let newArr = this.data.imageUrlList;
    newArr.splice(event.currentTarget.dataset.index,1);
    this.setData({
      imageUrlList:newArr
    })
    // 判断是否显示上传按钮
    if (this.data.imageUrlList.length < this.data.maxCount) {
      this.setData({
        showAdd: true
      })
    }
  },
  // 单张图片上传
  backImageUrl(path,data) {
    wx.showLoading({
      title: '',
    })
    let imageUrlArr = [];
    path.forEach(value => {
      if ((value.indexOf('tmp') == -1)){
        imageUrlArr.push(value.replace("https://rental-oss.oss-cn-shenzhen.aliyuncs.com/",""));
        if (imageUrlArr.length == path.length) {
          data.thumb = imageUrlArr.join('|');
          this.editRent(data);
        }
      }else {
        wx.uploadFile({
          url: API.USERRENTALUPLOADIMG,
          header: {
            "XX-Device-Type": "wxapp",
            "XX-Token": wx.getStorageSync('Token'),
            'content-type': 'multipart/form-data'
          },
          filePath: value,
          name: 'thumb',
          success:res=> {
            if(JSON.parse(res.data).code == 1){
              imageUrlArr.push(JSON.parse(res.data).data.thumb);
              if(imageUrlArr.length == path.length) {
                data.thumb = imageUrlArr.join('|');
                this.editRent(data);
              }
            }
            else if(JSON.parse(res.data).code == 10001) {
              toast.toast(JSON.parse(res.data).msg);
            }
            else {
              wx.showToast({
                title: JSON.parse(res.data).msg,
                icon: 'none',
              })
            }
          }
        })
      }
    })
  },
  // 单选框出租类型
  radioChange(event){
    this.setData({
      houseType:event.detail.value
    })
  },
  // 多选框标签类型
  checkboxChange(event) {
    this.setData({
      houseLabel:event.detail.value
    })
  },
  //入住时间选择
  bindDateChange: function (e) {
    this.setData({
      check_in_time: e.detail.value
    })
  },
  // 地区选择
  getPosition(event) {
    this.setData({
      region: event.detail.value
    })
  },
  // 第二个出租类型
  getRentType(event) {
    this.setData({
      typeIndex:event.detail.value
    })
  },
  // 获取地铁
  getSubway(event) {
    this.setData({
      subwayIndex:event.detail.value
    })
  },
  // 设备选择
  selectFacility(event) {
    this.setData({
      facilityIndex:event.detail.value
    })
  },
  // 手机号与微信相同
  phoneAndWechat(event){
    if(!event.detail.value.length){  
      this.setData({
        phoneAndWechat: 0
      })
    }
    else {
      this.setData({
        phoneAndWechat:1
      })
    }
  },
  // 选择房源
  selectStatus(event) {
    this.setData({
      status: parseInt(event.detail.value,10)
    })
  },
  // 提交表单
  submitForm(event) {
    let flag = true;
    let formData = event.detail.value
    let warn = '';
    if(formData.houseType == ''){
      warn = '请选择房源类型'
    }
    else if(formData.title == '') {
      warn = '请填写标题'
    }
    else if(formData.region.length == 0) {
      warn = '请选择地址'
    }
    else if (formData.detailPalce == '') {
      warn = '请填写详细地址'
    }
    else if (formData.description == '') {
      warn = '请填写房屋描述'
    }
    else if (formData.monthly == '') {
      warn = '请填写房租'
    }
    else if (formData.acreage == '') {
      warn = '请填写房屋面积'
    }
    else if (formData.mobile == '') {
      warn = '请填写手机号码'
    }
    else if (formData.deposit == '') {
      warn = '请填写押金'
    }
    else if (this.data.limit_tag.length == 0) {
      warn = '请选择标签'
    }
    else if (formData.facility.length == 0) {
      warn = '请选择设备'
    }
    else if (this.data.imageUrlList.length == 0) {
      warn = '请添加房屋图片'
    }
    else {
      flag = false;
      wx.showModal({
        title: '温馨提示',
        content: '确认修改？',
        success:result => {
          if(result.confirm){
            let data = {
              is_open_contact: this.data.isOpen,
              check_in_time: formData.check_in_time,
              id:this.data.id,
              source: parseFloat(formData.houseType),
              release_status:formData.status,
              title:formData.title,
              prov : formData.region[0],
              city : formData.region[1],
              area : formData.region[2],
              addr:formData.detailPalce,
              desc:formData.description,
              rent_type	:this.data.array[0][this.data.typeIndex[0]],
              room_type	:this.data.array[1][this.data.typeIndex[1]],
              subway	:this.data.subway[formData.subway],
              monthly	:parseFloat(formData.monthly),
              acreage	:formData.acreage,
              furniture:formData.facility.join("|"),
              mobile:formData.mobile,
              room	:formData.room,
              deposit	:formData.deposit,
              thumb:'',
              limit	:formData.limit_tag.join('|'),
              is_wechat_same_mobile	: this.data.phoneAndWechat,
              audit_status:this.data.status
            }
            console.log(this.data.imageUrlList)
            this.backImageUrl(this.data.imageUrlList,data);
          }
        }
      })
    }
    if(flag) {
      wx.showToast({
        title: warn,
        icon:'none'
      })
    }
  },
  //删除接口
  deleteHouse() {
    wx.showModal({
      title: '温馨提示',
      content: '确定删除？',
      success: result => {
        if(result.confirm) {
          wxPromise.wxPost(API.USERRENTALDELRENT,{
            id:this.data.id
          },wx.getStorageSync('Token'))
          .then(res=> {
            if(res.data.code == 1){
              wx.showToast({
                title: res.data.msg,
                icon:'none'
              })
            }
            else if(res.data.code == 10001){
              toast.toast(res.data.msg);
            }
            else {
              wx.showToast({
                title:res.data.msg,
                icon:'none'
              })
            }
          })
        }
      }
    })
  } ,

  // 出租修改接口
  editRent(data) {
    wxPromise.wxPost(API.USERRENTALEDITWXAPPLEASE,data,wx.getStorageSync('Token'))
    .then(res => {
      wx.hideLoading();
      if(res.data.code == 1) {
        wx.showToast({
          title: res.data.msg,
          icon:'none',
          success:() => {
            setTimeout(()=>{
              wx.navigateBack();
              const pages = getCurrentPages();
              const prePage = pages[pages.length - 2];
              prePage.setData({
                houseList: [],
                showLoading: true
              })
              prePage.getHouseList();
            },500)
          }
        })
      }
      else if(res.data.code == 10001){
        toast.toast(res.data.msg);
      }
      else {
        let warn = res.data.msg == undefined?'服务器异常':res.data.msg;
        wx.showToast({
          title: warn,
          icon:'none'
        })
      }
    })
  },
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      this.setData({
        subway: ['不限',...res.data.data.subway],
        facility: res.data.data.furniture,
        array: [res.data.data.rent_type_0, res.data.data.rent_type_1],
        limit_tag:res.data.data.limit_tag
      })
    })
  },
  // 取消按钮
  cancel() {
    wx.navigateBack();
  },
  // 获取详情的数据
  getHomeDetail(id) {
    wxPromise.wxPost(API.APIHOMERENTALRENTING_DETAILS, { id })
    .then(res => {
      let furnitureIndex = [];
      res.data.data.list.furniture.forEach(value=> {
        furnitureIndex.push(value.name);
      })
      let type_str = '';
      if(res.data.data.list.type_str == '个人出租') {
        type_str = 1;
      }else {
        type_str = 2
      }
      this.setData({
        showLoading: false,
        homeDetail: res.data.data.list,
        facilityIndex: furnitureIndex,
        subwayIndex: this.data.subway.indexOf(res.data.data.list.subway) == -1 ? 0 : this.data.subway.indexOf(res.data.data.list.subway),
        imageUrlList: res.data.data.list.thumb,
        houseLabel: res.data.data.list.limit,
        houseType: type_str,
        typeIndex:[(this.data.array[0].indexOf(res.data.data.list.rent_type)),(this.data.array[1].indexOf(res.data.data.list.room_type))],
        region: [res.data.data.list.prov, res.data.data.list.city, res.data.data.list.area],
        status: res.data.data.list.release_status,
        mobile: res.data.data.list.mobile,
        check_in_time: res.data.data.list.check_in_time.split(' ')[0]
      })
      // this.getContact();
    })
  },
  // 获取上一页信息 
  getPrePage() {
    const pages = getCurrentPages();
    const prePage = pages[pages.length -2];
    this.setData({
      homeTitle: prePage.data.houseList[this.data.index].title,
      audit_status: prePage.data.houseList[this.data.index].audit_status
    })
  },
  // 获取电话号码
  // getContact() {
  //   wxPromise.wxPost(API.USERPUBLICVIEWCONTACT, {
  //     id: this.data.id
  //   }, wx.getStorageSync('Token'))
  //     .then(res => {
  //       console.log(res);
  //       if (res.data.code == 1) {
  //         this.setData({
  //           mobile: res.data.data.mobile,
  //           phoneAndWechat: parseInt(res.data.data.is_wechat_same_mobile,10)
  //         })
  //       } else if(res.data.code == 10001){
  //         toast.toast(res.data.msg);
  //       }
  //       else {

  //       }
  //     })
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      host : API.COMMON,
      id:options.id,
      index:options.index
    })
    this.getconfig();
    this.getHomeDetail(this.data.id);
    this.getPrePage();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})