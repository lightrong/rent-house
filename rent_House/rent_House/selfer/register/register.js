const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const showToast = wxPromise.wxPromise(wx.showToast);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    verifyCode:'',
    phoneNumber:'',
    verifyCodeStatus:true,
    daojishi:60,
  },
  // 跳转登录页面
  getLogin(){
    wx.redirectTo({
      url: '../login/login',
    })
  },
  // 注册表单提交
  formSubmit(event){
    let flag = true;
    let warn = "";
    if (!event.detail.value.userName){
      warn = '请填写昵称';
    }
    else if (!(/^1[123456789]\d{9}$/.test(event.detail.value.phoneNumber))) {
      warn = '请输入正确的11位号码';
    } 
    else if (event.detail.value.verifyCode == '') { 
      warn = '请输入正确的6位验证码';
    } 
    else if(!event.detail.value.password){
      warn = '请输入密码';
    }
    else if (event.detail.value.password != event.detail.value.sure_password){
      warn = '两次密码不匹配，请重新输入';
    } 
    else {
      flag = false;
    }
    if(flag == true) {
      wx.showToast({
        title: warn,
        icon:'none'
      })
    }else {
      let data = {
        user_nickname: event.detail.value.userName,
        user_login: event.detail.value.phoneNumber,
        verify: event.detail.value.verifyCode,
        user_pass: event.detail.value.password,
        user_pass_confirm: event.detail.value.sure_password,
        openid: wx.getStorageSync('openid'),
        avatar: this.data.avatar
      }
      this.register(data);
    }
  },
  // 获取验证码
  getVerifyCode (event){
    if(this.data.verifyCodeStatus == false) return;
    if (/^1[123456789]\d{9}$/.test(this.data.phoneNumber)){
      this.sendCode(this.data.phoneNumber)
    }else {
      wx.showToast({
        title: '请输入正确的11位号码',
        icon:'none'
      })
    }
  },
  // 发送验证码
  sendCode(phoneNumber) {
    wx.showLoading({
      title: '正在发送',
    })
    wxPromise.wxPost(API.USERVERIFICATION_CODESEND,{
      user_login:phoneNumber
    })
    .then(res=> {
      if(res.data.code == 1){
        wx.hideLoading();
        wx.showToast({
          title: '发送成功',
          icon: 'none'
        })
        this.setData({
          verifyCodeStatus: !this.data.verifyCodeStatus
        })
        let time = 59;
        let timeId = setInterval( ()=> {
          this.setData({
            daojishi: time
          })
          if (time < 1) {
            clearInterval(timeId);
            this.setData({
              verifyCodeStatus: true,
              daojishi: 60
            })
          }
          time--;
        }, 1000);
      }
      else if(res.data.code == 0){
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
      else {
        wx.showToast({
          title: '服务器异常',
          icon: 'none'
        })
      }
    })
    .catch(err=> {
      wx.showToast({
        title: '服务器异常',
        icon:'none'
      })
    })
  },
  // 获取手机号码
  getPhoneNumber(event) {
    this.setData({
      phoneNumber:event.detail.value
    })
  },
  // 点击注册
  register(data) {
    wx.showLoading({
      title: '正在注册',
    })
    wxPromise.wxPost(API.USERPUBLICREGISTER,data)
    .then(res=> {
      wx.hideLoading();
      if(res.data.code == 1) {
        wx.setStorageSync('phone', data.user_login);
        setTimeout(()=>{
          showToast({
            title: res.data.msg,
            icon: 'none',
          },500)
          .then(() => {
            wx.redirectTo({
              url: '../login/login',
            })
          })
        })
      }
      else {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: function (res) {
              that.setData({
                avatar: res.userInfo.avatarUrl,
                userName: res.userInfo.nickName
              })
            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载,就是页面返回
   */
  onUnload: function () {
    // wx.switchTab({
    //   url: '../../pages/index/index',
    // })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})