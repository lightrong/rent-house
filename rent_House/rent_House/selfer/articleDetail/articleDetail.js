// selfer/articleDetail/articleDetail.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const utils = require('../../utils/util.js');
const WxParse = require('../../libs/wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    articleDetail:'',
    showLoading:true,
    showImage:false
  },

  // 获取文章详情
  getArticleDetail() {
    const that = this;
    wxPromise.wxPost(API.HOMERENTALARTICLE_DETAILS,{
      id:this.data.id
    })
    .then(res=> {
      this.setData({
        showLoading:false
      })
      if(res.data.code == 1){
        let obj = res.data.data;
        obj.create_time = utils.formatTime(new Date(obj.create_time*1000));
        this.setData({
          articleDetail:obj
        })
        if(typeof this.data.articleDetail.more == 'string') {
          this.setData({
            showImage:false
          })
        }else {
          this.setData({
            showImage: true
          })
        }
        // 富文本解析
        WxParse.wxParse('article', 'html', res.data.data.content, that, 10);
      }else {
        toast.toast(res.data.msg);
      }
    })
  },
  // 预览图片
  previewImage(event){
    if (!(typeof this.data.articleDetail.more == 'string')){
      let arr = [];
      for (let key in this.data.articleDetail.more) {
        arr.push(this.data.host + this.data.articleDetail.more[key])
      }    
      wx.previewImage({
        current:this.data.host + event.currentTarget.dataset.src,
        urls: arr
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id:options.id,
      host:'https://rental.homhere.cn/'
    })
    this.getArticleDetail();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})