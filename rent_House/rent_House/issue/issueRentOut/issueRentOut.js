const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const uploadFile = wxPromise.wxPromise(wx.uploadFile);
const getSetting = wxPromise.wxPromise(wx.getSetting);
const getLocation = wxPromise.wxPromise(wx.getLocation);
const authorize = wxPromise.wxPromise(wx.authorize);
// 引入SDK核心类
const QQMapWX = require('../../libs/qqmap-wx-jssdk1.0/qqmap-wx-jssdk.min.js');
const qqmapsdk = new QQMapWX({
  "key": 'K6BBZ-SSW3G-LFFQQ-I2C5P-GA42O-GVFAT'
});
var util = require("util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageUrlList:[],
    showAdd:true,
    maxCount:20,
    houseType:1,
    houseLabel:[],
    array:[],
    rentType:'',
    region:['北京市','北京市','朝阳区'],
    typeIndex:[0,0],
    subway:['不限'],
    subwayIndex:0,
    facility: {},
    facilityIndex:[],
    phoneAndWechat:0,
    limit_tag: [],
    mobile:'',
    dates: '',
    time:'',
    isOpen: 2 ,
    type:''
  },
  // Focus:function(e){
  //   if (e.detail.value == ""){
  //   }
  //   console.log("e",e)
  // },
  // Blur:function(e){

  // },
  //是否公开联系方式复选框
  isOpen: function (res) {
    if (this.data.isOpen == 2) {
      this.setData({
        isOpen: 1
      });
    } else {
      this.setData({
        isOpen: 2
      });
    }
  },
  // 联系方式选择 单选框
  lianxi: function (res) {
  },
  //入住时间选择
  bindDateChange: function (e) {
    this.setData({
      dates: e.detail.value 
    })
  },
  // 图片预览
  preview(event){
    wx.previewImage({
      current:event.currentTarget.dataset.image,
      urls: this.data.imageUrlList,
    })
  },
  // 上传图片
  uploadingImage (){
    const that = this;
    // 设置最大的上传张数
    that.setData({
      maxCount:that.data.maxCount - that.data.imageUrlList.length
    });
    // 选择相片
    wx.chooseImage({
      count: that.data.maxCount,
      success: res=> {
        that.setData({
          imageUrlList: [...that.data.imageUrlList, ...res.tempFilePaths],
          maxCount:20
        });
        // 判断是否显示上传按钮
        if (that.data.imageUrlList.length >= this.data.maxCount) {
          that.setData({
            showAdd:false
          })
        }
      },
    })
  },
  // 删除按钮
  deleteImg(event){
    let newArr = this.data.imageUrlList;
    newArr.splice(event.currentTarget.dataset.index,1);
    this.setData({
      imageUrlList:newArr
    })
    // 判断是否显示上传按钮
    if (this.data.imageUrlList.length < this.data.maxCount) {
      this.setData({
        showAdd: true
      })
    }
  },
  // 单张图片上传
  backImageUrl(path,data) {
    wx.showLoading({
      title: '',
    })
    let imageUrlArr = [];
    path.forEach(value => {
      wx.uploadFile({
        url: API.USERRENTALUPLOADIMG,
        header: {
          "XX-Device-Type": "wxapp",
          "XX-Token": wx.getStorageSync('Token'),
          'content-type': 'multipart/form-data'
        },
        filePath: value,
        name: 'thumb',
        success:res=> {
          if(JSON.parse(res.data).code == 1){
            imageUrlArr.push(JSON.parse(res.data).data.thumb);
            if(imageUrlArr.length == path.length) {
              data.thumb = imageUrlArr.join('|');
              this.rentalLease(data);
            }
          }
          else if(JSON.parse(res.data).code == 10001) {
            toast.toast(JSON.parse(res.data).msg);
          }
          else {
            wx.showToast({
              title: JSON.parse(res.data).msg,
              icon: 'none',
            })
          }
        }
      })
    })
  },
  // 单选框出租类型
  radioChange(event){
    this.setData({
      houseType:event.detail.value
    })
  },
  // 多选框标签类型
  checkboxChange(event) {
    this.setData({
      houseLabel:event.detail.value
    })
  },
  // 地区选择
  getPosition(event) {
    this.setData({
      region: event.detail.value
    })
  },
  // 第二个出租类型
  getRentType(event) {
    console.log(event)
    this.setData({
      typeIndex:event.detail.value,
      type: (this.data.array[0][event.detail.value[0]] || "") + (this.data.array[0][event.detail.value[0]] && this.data.array[1][event.detail.value[1]] ? "/" : "") + (this.data.array[1][event.detail.value[1]] || "")
    })
  },
  // 获取地铁
  getSubway(event) {
    this.setData({
      subwayIndex:event.detail.value
    })
  },
  // 设备选择
  selectFacility(event) {
    this.setData({
      facilityIndex:event.detail.value
    })
  },
  // 手机号与微信相同
  phoneAndWechat(event){
    if(!event.detail.value.length){  
      this.setData({
        phoneAndWechat: 0
      })
    }
    else {
      this.setData({
        phoneAndWechat:1
      })
    }
  },
  
  // 提交表单
  submitForm(event) {
    let flag = true;
    let formData = event.detail.value
    let warn = '';
    if(formData.houseType == ''){
      warn = '请选择房源类型'
    }
    else if(formData.title == '') {
      warn = '请填写标题'
    }
    else if(formData.region.length == 0) {
      warn = '请选择地址'
    }
    else if (formData.detailPalce == '') {
      warn = '请填写详细地址'
    }
    else if (formData.description == '') {
      warn = '请填写房屋描述'
    } else if (formData.monthly == '') {
      warn = '请填写房租'
    }
    else if (this.data.type == '' || this.data.type.split("/")[0] == "请选择" || this.data.type.split("/")[1] == "请选择") {
      warn = '请选择出租类型'
    }
    else if (formData.acreage == '') {
      warn = '请填写房屋面积'
    }
    else if (formData.mobile == '') {
      warn = '请填写手机号码'
    }
    else if (formData.deposit == '') {
      warn = '请填写押金'
    }
    else if (this.data.limit_tag.length == 0) {
      warn = '请选择标签'
    }
    else if (formData.facility.length == 0) {
      warn = '请选择设备'
    }
    else if (this.data.imageUrlList.length == 0) {
      warn = '请添加房屋图片'
    }
    else {
      flag = false;
      wx.showModal({
        title: '温馨提示',
        content: '确认发布？',
        success:result => {
          if(result.confirm){
            let facility = [];
            formData.facility.forEach(value=> {
              facility.push(this.data.facility[value].name);
            })
            let datas = {
              is_open_contact: this.data.isOpen,
              // is_open_contact:formData.is_open_contact,
              form_id: event.detail.formId,
              check_in_time:formData.check_in_time,
              source: parseFloat(formData.houseType),
              title:formData.title,
              prov : formData.region[0],
              city : formData.region[1],
              area : formData.region[2],
              addr:formData.detailPalce,
              desc:formData.description,
              rent_type	:this.data.array[0][this.data.typeIndex[0]],
              room_type	:this.data.array[1][this.data.typeIndex[1]],
              subway	:this.data.subway[formData.subway],
              monthly	:parseFloat(formData.monthly),
              acreage	:formData.acreage,
              furniture	:facility.join("|"),
              mobile:formData.mobile,
              room	:formData.room,
              deposit	:formData.deposit,
              thumb:'',
              limit	:formData.limit_tag.join('|'),
              is_wechat_same_mobile	: this.data.phoneAndWechat
            }
            this.backImageUrl(this.data.imageUrlList,datas);
          }
        }
      })
    }
    if(flag) {
      wx.showToast({
        title: warn,
        icon:'none'
      })
    }
  },
  // 发布接口
  rentalLease(data) {
    wxPromise.wxPost(API.USERRENTALWXAPPLEASE,data,wx.getStorageSync('Token'))
    .then(res => {
      wx.hideLoading();
      if(res.data.code == 1) {
        
        if(res.data.data.is_charge == 1) {
          wxPromise.wxPost(API.USERRENTALCREATEPAYDATAFORMINIAPP,{
            id:res.data.data.id
          },wx.getStorageSync('Token'))
          .then(pay=> {
            // 支付接口
            wx.requestPayment({
              'timeStamp': pay.data.data.result.timeStamp,
              'nonceStr': pay.data.data.result.nonceStr,
              'package': pay.data.data.result.package,
              'signType': pay.data.data.result.signType,
              'paySign': pay.data.data.result.paySign,
              'success': paysuccess=> {
                wx.showToast({
                  title: res.data.msg,
                  icon: 'none',
                  success: () => {
                    let url = '';
                    if (data.source == 1) {
                      url = '../../selfer/selfRent/selfRent'
                    }
                    else {
                      url = '../../selfer/businessRent/businessRent'
                    }
                    setTimeout(() => {
                      wx.navigateTo({
                        url,
                      });
                    }, 500)
                  }
                })
              },
              'fail': err=> {
                console.log('fail',err);
              }
            })
          })
            wx.showToast({
            title: res.data.msg,
            icon: 'none',
            success: () => {
              let url = '';
              if (data.source == 1) {
                url = '../../selfer/selfRent/selfRent'
              }
              else {
                url = '../../selfer/businessRent/businessRent'
              }
              setTimeout(() => {
                wx.navigateTo({
                  url,
                });
              }, 500)
            }
          })
        }
        else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            success: () => {
              let url = '';
              if (data.source == 1) {
                url = '../../selfer/selfRent/selfRent'
              }
              else {
                url = '../../selfer/businessRent/businessRent'
              }
              setTimeout(() => {
                wx.navigateTo({
                  url,
                });
              },500)
            }
          })}
      }
      else if(res.data.code == 10001){
        toast.toast(res.data.msg);
      }
      else {
        let warn = res.data.msg == undefined?'服务器异常':res.data.msg;
        wx.showToast({
          title: warn,
          icon:'none'
        })
      }
    })
  }, 
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      res.data.data.rent_type_0.unshift("请选择")
      res.data.data.rent_type_1.unshift("请选择")
      this.setData({
        subway: [...this.data.subway,...res.data.data.subway],
        facility: res.data.data.furniture,
        array: [res.data.data.rent_type_0, res.data.data.rent_type_1],
        limit_tag:res.data.data.limit_tag
      })
    })
  },
  // 取消按钮
  cancel() {
    wx.navigateBack();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      time: util.formatTime(new Date()),
      dates: util.formatTime(new Date()),
      host : API.COMMON,
      mobile:wx.getStorageSync('phone')
    })
    this.getconfig();
    // 可以通过 wx.getSetting 先查询一下用户是否授权了 "scope.userLocation" 这个 scope
    getSetting()
      .then(res => {
        if (!res.authSetting['scope.userLocation']) {
          authorize({
            scope: 'scope.userLocation'
          })
            .then(() => {
              getLocation()
            })
            .then(result => {
              // 你地址解析
              qqmapsdk.reverseGeocoder({
                location: {
                  latitude: result.latitude,
                  longitude: result.longitude
                },
                success: area => {
                  this.setData({
                    region: [
                      area.result.address_component.province, 
                      area.result.address_component.city,
                      area.result.address_component.district
                    ]
                  })
                },
                fail: area => {
                  console.log(area);
                }
              });
            })
        } else {
          getLocation()
            .then(result => {
              // 你地址解析
              qqmapsdk.reverseGeocoder({
                location: {
                  latitude: result.latitude,
                  longitude: result.longitude
                },
                success: area => {
                  this.setData({
                    region: [
                      area.result.address_component.province,
                      area.result.address_component.city,
                      area.result.address_component.district
                    ]
                  })
                },
                fail: area => {
                  console.log(area);
                }
              });
            })
        }
      })  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})