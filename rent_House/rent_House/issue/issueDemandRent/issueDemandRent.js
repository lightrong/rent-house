// 发布求租
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkInTime:'',
    addressData:[],
    houseType: 1,
    facility: [],
    facilityIndex: [],
    phoneAndWechat: false,
    region:[],
    mobile:'',
    isOpen: 2
  },
  //是否公开联系方式复选框
  isOpen: function (res) {
    if (this.data.isOpen == 2) {
      this.setData({
        isOpen: 1
      });
    } else {
      this.setData({
        isOpen: 2
      });
    }
  },
  // 联系方式选择 单选框
  lianxi:function(res){

  },
  // 单选框出租类型
  radioChange(event) {
    this.setData({
      houseType: event.detail.value
    })
  },
  // 选择时间
  selectTime(event) {
    this.setData({
      checkInTime:event.detail.value
    })
  },
  // 选择地区
  selectAddress(event) {
    this.setData({
      addressData:event.detail.value
    })
  },
  // 设备选择
  selectFacility(event) {
    this.setData({
      facilityIndex: event.detail.value
    })
  },
  // 手机号与微信相同
  phoneAndWechat(event) {
    if (!event.detail.value.length) {
      this.setData({
        phoneAndWechat: 0
      })
    }
    else {
      this.setData({
        phoneAndWechat: 1
      })
    }
  },
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      this.setData({
        facility: res.data.data.furniture,
      })
    })
  },
  //  表单提交
  submitForm(event) {
    let flag = true;
    let formData = event.detail.value
    let warn = '';
    if (formData.houseType == '') {
      warn = '请选择房源类型'
    }
    else if (formData.title == '') {
      warn = '请填写标题'
    }
    else if (formData.time == '') {
      warn = '请选择入住时间'
    }
    else if (formData.region.length == 0) {
      warn = '请选择地址'
    }
    else if (formData.description == '') {
      warn = '请填写房屋描述'
    }
    else if (formData.monthly == '') {
      warn = '请填写房租'
    }
    else if (formData.mobile == '') {
      warn = '请填写手机号码'
    }
    else if (formData.facility.length == 0) {
      warn = '请选择设备'
    }
    else {
      flag = false;
      wx.showModal({
        title: '温馨提示',
        content: '确定发布?', 
        success:result => {
          if(result.confirm) {
            let facility = [];
            formData.facility.forEach(value => {
              facility.push(this.data.facility[value].name);
            })
            let data = {
              is_open_contact: this.data.isOpen,
              // is_open_contact: formData.is_open_contact,
              source: parseFloat(formData.houseType),
              title: formData.title,
              prov: formData.region[0],
              city: formData.region[1],
              area: formData.region[2],
              desc: formData.description,
              monthly: parseFloat(formData.monthly),
              furniture: facility.join("|"),
              mobile: formData.mobile,
              check_in_time:formData.time,
              is_wechat_same_mobile: this.data.phoneAndWechat
            }
            this.issueDemandRent(data);
          }
        }
      })
    }
    if(flag){
      wx.showToast({
        title: warn,
        icon:'none'
      })
    }
  },
  // 发布求租
  issueDemandRent(data) {
    wx.showLoading({
      title: '',
    })
    wxPromise.wxGet(
      API.USERRENTALRENT,
      data,
      wx.getStorageSync('Token')
    ).then(res=> {
      wx.hideLoading();
      if(res.data.code ==10001) {
        toast.toast(res.data.msg);
      }
      else if(res.data.code == 1){
        wx.showToast({
          title: res.data.msg,
          icon:'none',
          success:()=> {
            setTimeout(()=> {
              wx.navigateTo({
                url: '../../selfer/selfDemand/selfDemand',
              })
            },500)
          }
        })
      }else {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
      }
    })
  },
  // 取消按钮
  cancel() {
    wx.navigateBack();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      host:API.COMMON,
      mobile:wx.getStorageSync('phone')
    })
    this.getconfig();
    this.setData({
      region:app.globalData.region
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})