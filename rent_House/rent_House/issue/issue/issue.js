const toast = require('../../utils/toast.js');
const API = require('../../utils/API.js');
Page({
  // 初始化数据
  data: {
    rentCheck: ''
  },
  // 发布出租
  getRentOut() {
    let that = this
    
    wx.request({
      url: API.USERRENTALCHECKRENTAL,
      data: {
        type: 1
      },
      header: {
        'XX-Device-Type': 'wxapp',
        'XX-Token': wx.getStorageSync('Token'),
        'content-type': 'multipart/form-data'
      },
      success(res) {
        console.log(res)
        that.setData({
          rentCheck: res.data.msg
        })

        if (wx.getStorageSync('Token')) {
          if (that.data.rentCheck == "success") {
            wx.showModal({
              title: '提示',
              content: '必须订阅消息才能发布',
              success(res) {
                if (res.confirm) {
                  // console.log('用户点击确定')
                  wx.requestSubscribeMessage({
                    tmplIds: ["Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM"],
                    success: (res) => {
                      if (res['Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM'] === 'accept') {
                        // wx.showToast({
                        //   title: '订阅OK！',
                        //   duration: 1000,
                        //   success(data) {
                        //     //成功
                        //     // resolve()
                        //     console.log(11111)
                        //   }
                        // })
                        wx.navigateTo({
                          url: '../issueRentOut/issueRentOut',
                        })
                      }
                    },
                    fail(err) {
                      //失败
                      console.error(err);
                      // reject()
                    }
                  })
                } else if (res.cancel) {
                  // console.log('用户点击取消')
                }
              }
            })
            
          } else {
            wx.showModal({
              title: '温馨提示',
              content: that.data.rentCheck,
              showCancel: false,
              confirmText: '知道了',
              success: function(res) {

              }
            })
          }
        } else {
          toast.toast('请先登录');
        }
      }
    })


  },
  // 发布求租
  getDemandRent() {
    let that = this
    wx.request({
      url: API.USERRENTALCHECKRENTAL,
      data: {
        type: 2
      },
      header: {
        'XX-Device-Type': 'wxapp',
        'XX-Token': wx.getStorageSync('Token'),
        'content-type': 'multipart/form-data'
      },
      success(res) {
        console.log(res)
        that.setData({
          rentCheck: res.data.msg
        })

        if (wx.getStorageSync('Token')) {
          if (that.data.rentCheck == "success") {
            wx.showModal({
              title: '提示',
              content: '必须订阅消息才能发布',
              success(res) {
                if (res.confirm) {
                  // console.log('用户点击确定')
                  wx.requestSubscribeMessage({
                    tmplIds: ["Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM"],
                    success: (res) => {
                      if (res['Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM'] === 'accept') {
                        // wx.showToast({
                        //   title: '订阅OK！',
                        //   duration: 1000,
                        //   success(data) {
                        //     //成功
                        //     // resolve()
                        //     console.log(11111)
                        //   }
                        // })
                        wx.navigateTo({
                          url: '../issueDemandRent/issueDemandRent',
                        })
                      }
                    },
                    fail(err) {
                      //失败
                      console.error(err);
                      // reject()
                    }
                  })
                } else if (res.cancel) {
                  // console.log('用户点击取消')
                }
              }
            })
            
          } else {
            console.log(that.data.rentCheck)
            wx.showModal({
              title: '温馨提示',
              content: that.data.rentCheck,
              showCancel: false,
              confirmText: '知道了',
              success: function(res) {}
            })

          }
        } else {
          toast.toast('请先登录');
        }
      }
    })
  },
  // 发布文章
  getArticle() {
    if (wx.getStorageSync('Token')) {
      wx.showModal({
        title: '提示',
        content: '必须订阅消息才能发布',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
            wx.requestSubscribeMessage({
              tmplIds: ["Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM"],
              success: (res) => {
                if (res['Qcrp_F3QyyJ5EKPvh3xYDmdgrnAvTPykTkTkn867VXM'] === 'accept') {
                  // wx.showToast({
                  //   title: '订阅OK！',
                  //   duration: 1000,
                  //   success(data) {
                  //     //成功
                  //     // resolve()
                  //     console.log(11111)
                  //   }
                  // })
                  wx.navigateTo({
                    url: '../issueArticle/issueArticle',
                  })
                }
              },
              fail(err) {
                //失败
                console.error(err);
                // reject()
              }
            })
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
      
    } else {
      toast.toast('请先登录');
    }
  },
  // 监听页面加载
  onLoad() {},
  onShareAppMessage() {

  }
});