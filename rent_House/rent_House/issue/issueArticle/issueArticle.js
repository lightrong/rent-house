// issue/issueArticle/issueArticle.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const showModal = wxPromise.wxPromise(wx.showModal);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageUrlList: [],
    showAdd: true,
    maxCount: 4,
  },
  // 上传图片
  uploadingImage() {
    const that = this;
    // 设置最大的上传张数
    that.setData({
      maxCount: that.data.maxCount - that.data.imageUrlList.length
    });
    // 选择相片
    wx.chooseImage({
      count: that.data.maxCount,
      success: res => {
        that.setData({
          imageUrlList: [...that.data.imageUrlList, ...res.tempFilePaths],
          maxCount: 4
        });
        // 判断是否显示上传按钮
        if (that.data.imageUrlList.length >= 4) {
          that.setData({
            showAdd: false
          })
        }
      },
    })
  },
  // 图片上传
  upLoadImage (path,data) {
    wx.showLoading({
      title: '上传中',
    })
    let arr = []
    path.forEach(value=> {
      wx.uploadFile({
        url: API.USERRENTALUPLOADIMG,
        filePath: value,
        name: 'thumb',
        header: {
          "XX-Device-Type": "wxapp",
          "XX-Token": wx.getStorageSync('Token'),
          'content-type': 'multipart/form-data'
        },
        success:res=> {
          arr.push(JSON.parse(res.data).data.thumb);
          if(arr.length == path.length) {
            data.more = arr.join('|');
            this.addArticle(data);
          }
        }
      })
    })
  },
  // 删除按钮
  deleteImg(event) {
    let newArr = this.data.imageUrlList;
    newArr.splice(event.currentTarget.dataset.index, 1);
    this.setData({
      imageUrlList: newArr
    })
    // 判断是否显示上传按钮
    if (this.data.imageUrlList.length < 4) {
      this.setData({
        showAdd: true
      })
    }
  },

  // 发布按钮
  submitForm(event) {
    let flag = true;
    let warn = '';
    if(event.detail.value.title == '') {
      warn = '请填写文章标题'
    }
    else if(event.detail.value.content == '') {
      warn = '请填写内容'
    }
    else {
      flag = false ;
      showModal({
        title: '温馨提示',
        content: '确定发布？',
      })
      .then(res => {
        if(res.confirm) {
          const data = {
            post_title: event.detail.value.title,
            post_content: event.detail.value.content,
            more:''
          }
          this.upLoadImage(this.data.imageUrlList,data);
        }
      })
    }
    if(flag) {
      wx.showToast({
        title: warn,
        icon: 'none',
      })
    }
  },
  // 发布文章接口
  addArticle(data) {
    // wx.uploadFile({
    //   url: API.USERPERSONALARTICLE_POST_WXAPP,
    //   filePath: path,
    //   header:{
    //     "XX-Device-Type": "wxapp",
    //     "XX-Token": wx.getStorageSync('Token'),
    //     'content-type': 'multipart/form-data'
    //   },
    //   name: 'more',
    //   formData: data,
    //   success:res => {
    //     console.log(res);
    //     if(JSON.parse(res.data).code == 1) {
    //       wx.showToast({
    //         title: JSON.parse(res.data).msg,
    //         icon:'none',
    //         success:() => {
    //           setTimeout(()=> {  
    //             wx.redirectTo({
    //               url: '../../selfer/articleManage/articleManage',
    //             })
    //           },500)
    //         }
    //       })
    //     }
    //     else if (JSON.parse(res.data).code == 10001) {
    //       toast.toast(JSON.parse(res.data).msg);
    //     }
    //     else {
    //       wx.showToast({
    //         title: JSON.parse(res.data).msg,
    //         icon:'none'
    //       })
    //     }
    //   }
    // })
    wxPromise.wxGet(API.USERPERSONALARTICLE_POST_WXAPP, 
      data, 
      wx.getStorageSync('Token')
    ).then(res=> {
      wx.hideLoading();
      if (res.data.code == 1) {
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          success: () => {
            setTimeout(() => {
              wx.redirectTo({
                url: '../../selfer/articleManage/articleManage',
              })
            }, 500)
          }
        })
      }
      else if (res.data.code == 10001) {
        toast.toast(res.data.msg);
      }
      else {
        wx.showToast({
          title: res.data.msg || '服务器异常',
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id){
      this.setData({
        id:options.id
      }) 
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})