const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const app = getApp();
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: ['出租类型', '个人出租', '个人求租', '公寓出租'],
    area: ['选择地区'],
    price: [],
    typeData: '',
    areaData: '',
    priceData: '',
    more: [{
        title: '标签',
        name: 'label',
        content: ['不限'],
        labelSelect: null
      },
      {
        title: '类型',
        name: 'type',
        content: ['不限'],
        labelSelect: null
      },
      {
        title: '地铁',
        name: 'subway',
        content: ['不限'],
        labelSelect: null
      },
      {
        title: '排序',
        name: 'sort',
        content: ['时间排序', '点赞排序'],
        labelSelect: null
      }
    ],
    house_resource: [],
    filterItem: [],
    isShow: false,
    isFilter: '',
    isSelect: null,
    filter: '',
    currentProvince: '',
    currentArea: '',
    tip: false,
    limit: '',
    rent_type: '',
    subway: '',
    title: '',
    sort: 'release_time',
    region: [],
    iPhoneX:false
  },
  fabuBtn() {
    wx.switchTab({
      url: '/issue/issue/issue',
    })
  },
  // 筛选器信息
  getFilterInfo(event) {
    let filter = event.currentTarget.dataset.filter;
    this.setData({
      filter
    })
    if (this.data.isFilter == '' || this.data.isFilter == undefined || this.data.isFilter == null) {
      this.setData({
        isFilter: filter,
        isShow: true
      })
    } else if (this.data.isFilter == filter) {
      this.setData({
        isShow: false,
        isFilter: '',
        filterItem: [],
        isSelect: null
      })
      return;
    } else {
      this.setData({
        isShow: true,
        isFilter: filter
      })
    }
    this.setData({
      filterItem: this.data[filter],
      isSelect: filter
    })
  },
  // 选择数据
  selectData(event) {

    if (this.data.filter == "more") return;
    page = 1;
    this.setData({
      [this.data.filter + "Data"]: event.currentTarget.dataset.select,
      house_resource: [],
      tip: false,
      title: ''
    })
    if (event.currentTarget.dataset.select.indexOf('个人出租') != -1) {
      this.setData({
        sort: "like_count"
      })
    } else if (event.currentTarget.dataset.select.indexOf('公寓出租') != -1) {
      this.setData({
        sort: "release_time"
      })
    } else if (event.currentTarget.dataset.select.indexOf('求租') != -1) {
      this.setData({
        sort: "release_time"
      })
    }
    const data = {
      class: this.formatType(this.data.typeData),
      city: this.citySort(this.data.currentProvince, this.data.currentArea),
      area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
      monthly: this.formatPrice(this.data.priceData),
      page: page,
      limit: this.data.limit,
      rent_type: this.data.rent_type,
      subway: this.data.subway,
      order: this.data.sort,
    }
    this.getSearchList(data);
  },
  // 隐藏筛选器
  hiddenFilter() {
    this.setData({
      isShow: false,
      isSelect: null,
      isFilter: '',
    })
  },
  // 更多里的单选框选择
  radioChange(event) {
    let newMore = this.data.more;
    newMore[event.target.dataset.index].labelSelect = event.detail.value;
    this.setData({
      more: newMore
    })
  },
  // 获取配置参数
  getconfig(fn) {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
      .then(res => {
        let arr = this.data.more;
        arr[0].content = [...arr[0].content, ...res.data.data.limit_tag];
        arr[1].content = [...arr[1].content, ...res.data.data.rent_type_0];
        arr[2].content = [...arr[2].content, ...res.data.data.subway];
        const pages = getCurrentPages();
        const prePage = pages[pages.length - 2];
        this.setData({
          price: ['价格区间', ...res.data.data.rent_price],
          typeData: this.data.type[0],
          priceData: '价格区间',
          areaData: this.data.area[0],
          more: arr
        })
        if (typeof fn == "function") {
          fn();
        }
      })
  },
  // 根据市获取地区
  getArea(fn) {
    wxPromise.wxGet(API.HOMESEARCHGETAREA, {
        city: this.data.currentArea
      })
      .then(res => {
        res.data.data.unshift('选择地区');
        this.setData({
          area: res.data.data,
        })
        if (typeof fn == "function") {
          fn(res);
        }
      })

  },
  // 获取城市
  getPosition(event) {
    this.setData({
      currentProvince: event.detail.value[0],
      currentArea: event.detail.value[1],
      house_resource: []
    })
    let data = {
      class: this.formatType(this.data.typeData),
      area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
      city: this.citySort(this.data.currentProvince, this.data.currentArea),
      monthly: this.formatPrice(this.data.priceData),
      page: page,
      limit: this.data.limit,
      rent_type: this.data.rent_type,
      subway: this.data.subway,
      order: this.data.sort
    }
    this.getSearchList(data);
    this.getArea(res => {
      this.setData({
        areaData: res.data.data[0]
      })
    });
  },
  // 更多的确定按钮
  submitForm(event) {
    page = 1;
    this.setData({
      limit: event.detail.value.label == '不限' ? '' : event.detail.value.label,
      rent_type: event.detail.value.type == '不限' ? '' : event.detail.value.type,
      subway: event.detail.value.subway == '不限' ? '' : event.detail.value.subway,
      sort: event.detail.value.sort == '时间排序' || event.detail.value.sort == '' ? "release_time" : "like_count",
      house_resource: [],
      tip: false,
      title: ''
    })
    const data = {
      class: this.formatType(this.data.typeData),
      area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
      city: this.citySort(this.data.currentProvince, this.data.currentArea),
      monthly: this.formatPrice(this.data.priceData),
      page: page,
      limit: this.data.limit,
      rent_type: this.data.rent_type,
      subway: this.data.subway,
      order: this.data.sort
    }
    this.getSearchList(data);
    this.hiddenFilter();
  },
  // 重置
  submitReset(event) {
    let more = this.data.more;
    more.forEach(value => {
      value.labelSelect = null;
    })
    this.setData({
      more
    })
  },
  // 阻止冒泡
  catchEvent() {},

  // 获取搜索列表
  getSearchList(data, fn) {
    wx.showLoading({
      title: '',
    })
    // 发送请求
    wxPromise.wxGet(API.HOMESEARCHINDEX, data)
      .then(res => {
        wx.hideLoading();
        if (typeof fn == "function") {
          fn(res);
        }
        if (res.data.data.data.length && res.data.data.current_page == 1) {
          for (let key in res.data.data.ad) {
            res.data.data.data.splice(key - 1, 0, res.data.data.ad[key]);
          }
        }
        this.setData({
          house_resource: [...this.data.house_resource, ...res.data.data.data],
        })
      })
  },
  // 搜索内容 
  getSearchTitle(event) {
    this.setData({
      title: event.detail.value,
    })
  },
  // 按钮搜索
  iconSearch() {
    if (!this.data.title) return
    this.setData({
      house_resource: []
    })
    const data = {
      title: this.data.title,
      class: this.formatType(this.data.typeData),
      city: this.citySort(this.data.currentProvince, this.data.currentArea),
      area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
      monthly: this.formatPrice(this.data.priceData),
      limit: this.data.limit,
      rent_type: this.data.rent_type,
      subway: this.data.subway,
      order: this.data.sort
    }
    this.getSearchList(data);
  },
  // 城市格式化
  formatCity(city) {
    let str = city;
    let newCity = '';
    if (str) {
      newCity = str.replace(/[市省]$/g, '');
    }
    return newCity;
  },
  // 城市筛选 
  citySort(currentProvince, currentArea) {
    const cp = this.formatCity(currentProvince) || "";
    const ca = this.formatCity(currentArea) || "";
    return (cp + ((cp == "" || ca == "") ? "" : "/") + ca)
  },
  // 几个区间
  formatPrice(str) {
    if (str.indexOf('以下') != -1) {
      const newStr = str.split('以下');
      return "0," + newStr[0]
    } else if (str.indexOf('以上') != -1) {
      const newStr = str.split('以上');
      return newStr[0] + ",0"
    } else if (str == '价格区间') {
      return '';
    } else {
      const newStr = str.split('-');
      return newStr[0] + "," + newStr[1]
    }
  },
  // 出租类型格式化
  formatType(str) {
    if (str == "个人出租") {
      return '1,1';
    } else if (str == '个人求租') {
      return '1,2';
    } else if (str == '公寓出租') {
      return '2,1';
    } else if (str == '公寓求租') {
      return '2,2'
    } else {
      return ''
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that=this
    wx.getSystemInfo({
      success: function (res) {
        if (res.windowWidth === 375 && res.windowHeight==724){
          that.setData({
            iPhoneX: true
          })
        }
      },
    })

    if (options.source) {
      //链接进入
      page = 1;
      const pages = getCurrentPages();
      const prePage = pages[pages.length - 2];
      this.setData({
        currentArea: options.city,
        typeData: this.data.type[0],
        region: app.globalData.region || [],
        currentProvince: options.prov
      })
      this.getArea();
      this.getconfig(() => {
        this.setData({
          typeData: options.type,
          areaData: options.area
        })
        if (this.data.typeData.indexOf('个人出租') != -1) {
          this.setData({
            sort: "like_count"
          })
        } else if (this.data.typeData.indexOf('求租') != -1) {
          this.setData({
            sort: "release_time"
          })
        } else if (this.data.typeData.indexOf('公寓出租') != -1) {
          this.setData({
            sort: "release_time"
          })
        }
        const data = {
          class: this.formatType(this.data.typeData),
          area: this.data.areaData,
          city: this.citySort(this.data.currentProvince, this.data.currentArea),
          monthly: this.formatPrice(this.data.priceData),
          order: this.data.sort,
          page: page,
        }
        this.getSearchList(data);
      });


    } else {
      if (options.type) {
        //  首页进入
        page = 1;
        const pages = getCurrentPages();
        const prePage = pages[pages.length - 2];
        this.setData({
          currentArea: app.globalData.currentCity || '',
          typeData: this.data.type[0],
          region: app.globalData.region || [],
          currentProvince: app.globalData.region[0]
        })
        this.getArea();
        this.getconfig(() => {
          this.setData({
            typeData: options.type,
          })
          if (this.data.typeData.indexOf('个人出租') != -1) {
            this.setData({
              sort: "like_count"
            })
          } else if (this.data.typeData.indexOf('求租') != -1) {
            this.setData({
              sort: "release_time"
            })
          } else if (this.data.typeData.indexOf('公寓出租') != -1) {
            this.setData({
              sort: "release_time"
            })
          }
          const data = {
            class: this.formatType(this.data.typeData),
            // area: this.data.areaData,
            city: this.citySort(this.data.currentProvince, this.data.currentArea),
            monthly: this.formatPrice(this.data.priceData),
            order: this.data.sort,
            page: page,
          }
          this.getSearchList(data);
        });
      } else if (options.title) {
        const data = {
          title: options.title,
          city: this.citySort(this.data.currentProvince, this.data.currentArea),
        }
        this.setData({
          title: options.title
        })
        this.getconfig();
        this.getSearchList(data);
      } else {
        const data = {
          order: this.data.sort,
          city: this.citySort(this.data.currentProvince, this.data.currentArea),
          page: page,
        }
        this.getconfig();
        this.getSearchList(data);
      }

    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    page = 1;
    let data = '';
    this.setData({
      house_resource: []
    })
    if (this.data.title) {
      data = {
        class: this.formatType(this.data.typeData),
        title: this.data.title,
        city: this.citySort(this.data.currentProvince, this.data.currentArea),
        area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
        monthly: this.formatPrice(this.data.priceData),
        page: page,
        limit: this.data.limit,
        rent_type: this.data.rent_type,
        subway: this.data.subway,
        order: this.data.sort
      }
    } else {
      data = {
        class: this.formatType(this.data.typeData),
        area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
        city: this.citySort(this.data.currentProvince, this.data.currentArea),
        monthly: this.formatPrice(this.data.priceData),
        page: page,
        limit: this.data.limit,
        rent_type: this.data.rent_type,
        subway: this.data.subway,
        order: this.data.sort
      }
    }
    this.getSearchList(data);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    page++;
    let data = "";
    if (this.data.title) {
      data = {
        class: this.formatType(this.data.typeData),
        title: this.data.title,
        city: this.citySort(this.data.currentProvince, this.data.currentArea),
        area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
        monthly: this.formatPrice(this.data.priceData),
        page: page,
        limit: this.data.limit,
        rent_type: this.data.rent_type,
        subway: this.data.subway,
        order: this.data.sort
      }
    } else {
      data = {
        class: this.formatType(this.data.typeData),
        area: this.data.areaData == '选择地区' ? '' : this.data.areaData,
        city: this.citySort(this.data.currentProvince, this.data.currentArea),
        monthly: this.formatPrice(this.data.priceData),
        page: page,
        limit: this.data.limit,
        rent_type: this.data.rent_type,
        subway: this.data.subway,
        order: this.data.sort
      }
    }
    this.getSearchList(data, res => {
      if (res.data.data.data.length == 0) {
        this.setData({
          tip: true
        })
      } else {
        this.setData({
          tip: false
        })
      }
    });
  },
})