const app = getApp();
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const getSetting = wxPromise.wxPromise(wx.getSetting);
const getLocation = wxPromise.wxPromise(wx.getLocation);
const authorize = wxPromise.wxPromise(wx.authorize);
// 引入SDK核心类
const QQMapWX = require('../../libs/qqmap-wx-jssdk1.0/qqmap-wx-jssdk.min.js');
const qqmapsdk = new QQMapWX({
  "key": 'K6BBZ-SSW3G-LFFQQ-I2C5P-GA42O-GVFAT'
});
// const TxMap = wxPromise.wxPromise(qqmapsdk.reverseGeocoder);
Page({
  data: {
    lunbo:[],
    ad1:[],
    ad2:[],
    ad3:[],
    ad4:[],
    host:'',
    rent_1_1:[],
    rent_1_2:[],
    rent_2_1:[],
    rent_2_2:[],
    selfer:[],
    business:[],
    portal:[],
    s_active:'rent_1_1',
    b_active:'rent_1_2',
    indicatorDots: true,
    autoplay: true,
    interval: 4000,
    duration: 500, 
    showLoading:true,
    currentArea:'',
    typeData: ['个人出租', '个人求租','公寓出租'],
    typeIndex:0,
    sanjiao:false,
    district:'',
    region:[],
    houseType:'个人出租'
  },
  fabuBtn(){
    wx.switchTab({
      url: '/issue/issue/issue',
    })
  },
  // 加载更多
  uploadMore() {
    wx.requestSubscribeMessage({
      tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
      success: (res) => {
        if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
          // wx.showToast({
          //   title: '订阅OK！',
          //   duration: 1000,
          //   success(data) {
          //     //成功
          //     // resolve()
          //     console.log(11111)
          //   }
          // })
        }
      },
      fail(err) {
        //失败
        console.error(err);
        // reject()
      }
    })
    wx.navigateTo({
      url: '/pages/searchList/searchList?type=公寓出租',
    })
  },
  // 城市格式化
  formatCity(city) {
    const newCity = city?city.replace(/市$/g, ''):'';
    return newCity;
  },
  // 获取主页数据
  getHomeIndex(city) {
    // 发送请求
    console.log("1111",city)
    wxPromise.wxGet(API.HOMEINDEX,{
      city: this.formatCity(city)
    })
    .then(res=>{
      this.setData({
        showLoading:false,
        lunbo:res.data.data.lunbo,
        ad1:res.data.data.ad1,
        ad2:res.data.data.ad2,
        ad3:res.data.data.ad3,
        ad4:res.data.data.ad4,
        rent_1_1:res.data.data.rent_1_1,
        rent_1_2:res.data.data.rent_1_2,
        rent_2_1:res.data.data.rent_2_1,
        rent_2_2:res.data.data.rent_2_2,
        selfer: res.data.data[this.data.s_active],
        business: res.data.data[this.data.b_active],
        portal: res.data.data.portal,
      })
    })
  },
  // 个人出租求租切换
  selfer(event) {
    let type = ''
    if(event.currentTarget.dataset.type == 'rent_1_1'){
      type = '个人出租'
    }
    else {
      type = '个人求租'
    }
    this.setData({
      selfer:this.data[event.currentTarget.dataset.type],
      s_active:event.currentTarget.dataset.type,
      houseType : type
    })
  },
  // 商业出租求租切换
  business(event) {
    this.setData({
      business:this.data[event.currentTarget.dataset.type],
      b_active:event.currentTarget.dataset.type
    })
  },
  gengduoBtn(){
    wx.requestSubscribeMessage({
      tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
      success: (res) => {
        if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
          // wx.showToast({
          //   title: '订阅OK！',
          //   duration: 1000,
          //   success(data) {
          //     //成功
          //     // resolve()
          //     console.log(11111)
          //   }
          // })
        }
      },
      fail(err) {
        //失败
        console.error(err);
        // reject()
      }
    })
    wx.navigateTo({
      url: '../searchList/searchList?type=' + this.data.houseType,
      success: res => {
      }
    })
  },
  // 进去搜索页面
  getSearchList(event) {
    wx.requestSubscribeMessage({
      tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
      success: (res) => {
        if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
          // wx.showToast({
          //   title: '订阅OK！',
          //   duration: 1000,
          //   success(data) {
          //     //成功
          //     // resolve()
          //     console.log(11111)
          //   }
          // })
        }
      },
      fail(err) {
        //失败
        console.error(err);
        // reject()
      }
    })
    wx.navigateTo({
      url: '../searchList/searchList?type=' + this.data.typeData[this.data.typeIndex] + '&city=' + this.data.currentArea,
      success: res=> {
      }
    })
  },
  // 选择地址
  getPosition(event) {
    this.setData({
      currentArea:event.detail.value[1]
    })
    this.getHomeIndex(this.data.currentArea);
  },
  // 选择租房类型
  getHouseType(event) {
    this.setData({
      sanjiao:!this.data.sanjiao,
      typeIndex:event.detail.value
    })
  },
  // 进入文章列表
  gotoArticle () {
    wx.requestSubscribeMessage({
      tmplIds: ["mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA"],
      success: (res) => {
        if (res['mIxlD4utuXaH2PKC_Qrq-oXY8ARj2G9pRyvkQvjsKBA'] === 'accept') {
          // wx.showToast({
          //   title: '订阅OK！',
          //   duration: 1000,
          //   success(data) {
          //     //成功
          //     // resolve()
          //     console.log(11111)
          //   }
          // })
        }
      },
      fail(err) {
        //失败
        console.error(err);
        // reject()
      }
    })
    wx.navigateTo({
      url: '../articleList/articleList',
    })
  },
  // 取消选择租房类型
  cancelHouseType() {
    this.setData({
      sanjiao: !this.data.sanjiao
    })
  },
  // 改变三角形
  changeSanjiao(){
    this.setData({
      sanjiao:!this.data.sanjiao
    })
  },
  // 获取文章详情页
  getArticleDetail (event) {
    wx.navigateTo({
      url: '../../selfer/articleDetail/articleDetail?id=' + event.currentTarget.id,
    })
  },
  // 监听页面加载
  onLoad() {
    // 图片前缀
    this.setData({
      host:API.COMMON
    })
    // 可以通过 wx.getSetting 先查询一下用户是否授权了 "scope.userLocation" 这个 scope
    getSetting()
    .then(res=> {
      if (!res.authSetting['scope.userLocation']) {
        authorize({
          scope: 'scope.userLocation'
        })
        .then(errMsg=> {
          app.getLocation(this);
        })
        .catch(fail=> {
          wx.showModal({
            title: '是否授权当前位置',
            content: '需要获取您的地理位置，请确认授权，否则相关功能将无法使用',
            success: data => {
              if (data.confirm) {
                wx.openSetting({
                  success: result => {
                    if (result.authSetting['scope.userLocation'] === true) {
                      app.getLocation(this);
                    }
                    else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        success: () => {
                          this.getHomeIndex(this.data.currentArea);
                        }
                      })
                    }
                  }
                })
              }
              else if(data.cancel) {
                this.getHomeIndex(this.data.currentArea);
              }
            }
          })
        })
      } 
      else {
        app.getLocation(this);
      }
    })
    .catch(res => {
      console.log('authorize', res);
      this.getHomeIndex(this.data.currentArea);
    })
  }, 
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      showLoading: true
    })
    this.getHomeIndex(this.data.currentArea);
  },
  // 分享
  onShareAppMessage() {

  }
})
