// 用户界面
const API = require('../../utils/API.js')
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    star:3,
    type:1,
    page:1,
    rentList:[],
    userid:'',
    tag:[],
    tagCount:[],
    host:'',
    labelList:[],
    starCount:1,
    staticPage : [1,2,3,4],
    last_page:1
  },
  // 选择类型
  selectType(event) {
    this.setData({
      page:1,
      type : event.currentTarget.dataset.type
    })
    if (parseInt(event.currentTarget.dataset.type,10) == 3){
      this.getArticle();
    }
    else {
      this.getRentList();
    }
  },
  // 获取出租、求租列表
  getRentList() {
    wx.showLoading({
      title: '',
    })
    wxPromise.wxPost(
      API.APIHOMERENTALRENTING_LIST,
      {
        userid : this.data.userid,
        type : this.data.type,
        page:this.data.page,
        pagesize : 4
      }
    ).then( res=> {
      wx.hideLoading();
      if(res.data.code == 1){
        let arr = [];
        let data = res.data.data.last_page >= 5 ? 5 : res.data.data.last_page
        for(let i = 1;i <= data; i++){
          arr.push(i);
        }
        this.setData({
          rentList: res.data.data.data,
          last_page:res.data.data.last_page,
          staticPage:arr
        })
      }else {
        toast.toast(res.data.msg)
      }
    })
  },
  // 获取用户信息
  getUserInfo() {
    wxPromise.wxGet(
      API.HOMERENTALINDEX,
      {
        userid:this.data.userid
      }
    ).then(res=> {
      if(res.data.code == 1) {
        const label = res.data.data.tag;
        let labelName = [],labelCount = [];
        for(var key in label) {
          labelName.push(key);
          labelCount.push(label[key]);
        }
        this.setData({
          userInfo:res.data.data,
          star:res.data.data.star,
          tag:labelName,
          tagCount:labelCount
        })
        this.commentContent.readyComplete();
      }else {
        toast.toast(res.data.msg);
      }
    })
  },
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      this.setData({
        labelList: res.data.data.user_comment_tag
      })
    })
  },
  // 获取文章
  getArticle() {
    wx.showLoading({
      title: '',
    })
    wxPromise.wxGet(
      API.HOMERENTALARTICLE_LIST,
      {
        userid: this.data.userid,
        page:this.data.page
      },
    ).then(res=> {
      wx.hideLoading();
      if (res.data.code == 1) {
        let arr = [];
        let data = res.data.data.last_page >= 5 ? 5 : res.data.data.last_page
        for (let i = 1; i <= data; i++) {
          arr.push(i);
        }
        this.setData({
          rentList: res.data.data.data,
          last_page: res.data.data.last_page,
          staticPage: arr
        })
      }
    })
  },
  // 获取文章详情页
  getArticleDetail(event) {
    wx.navigateTo({
      url: '../../selfer/articleDetail/articleDetail?id=' + event.currentTarget.id,
    })
  },
  // 评论操作
  _comment() {
    this.getUserInfo();
    this.setData({
      starCount:1
    })
  },
  // 组件完成渲染触发
  readyEvent() {
    this.commentContent = this.selectComponent("#comment");
  },
  // 分页器
  // prev 上一页
  prev() {
    if(this.data.page <= 1) return;
    this.setData({
      page: this.data.page - 1
    })
    this.getRentList();
  },
  // 选中哪一页
  selectPage(event) {
    this.setData({
      page : event.currentTarget.dataset.index
    })
    this.getRentList();
  },
  // 下一页
  next () {
    if(this.data.page >= this.data.last_page) return;
    this.setData({
      page: this.data.page + 1
    })
    this.getRentList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userid : options.userid,
      host: API.COMMON
    })
    this.getRentList();
    this.getUserInfo();
    this.getconfig();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})