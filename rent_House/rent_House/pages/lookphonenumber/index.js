// pages/lookphonenumber/index.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:"",
    copy:'',
    id:1,
    mobile:"",
    show:true,
    name:"",
    avatarUrl:""
  },
  report(){
    wx.navigateTo({
      url: '/pages/report/report?homeId='+this.data.id,
    })
  },
  onGotUserInfo(e){
    var that=this
    this.setData({
      show:false,
      name: e.detail.userInfo.nickName,
      avatarUrl: e.detail.userInfo.avatarUrl
    })
    console.log(e.detail.userInfo)
    wx.login({
      success(res) {
        if (res.code) {
          //发起网络请求
          wxPromise.wxGet(API.HOMERENTALIFRENTALSTATUS, {
            id: that.data.id,
            code: res.code,
            nickname: that.data.name,
            avatar: that.data.avatarUrl
          })
            .then(res => {
              console.log(res)
              if (res.data.code == 1) {
                that.setData({
                  phone: res.data.data.mobile
                })
              } else if (res.data.code == 102) {
                wx.showModal({
                  title: '提示',
                  content: '该房源已下架',
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.reLaunch({
                        url: '/pages/index/index',
                      })
                    }
                  }
                })
              } else if (res.data.code == 101){
                wx.reLaunch({
                  url: '/pages/rentOut/rentOut?id=' + that.data.id,
                })
              }
            })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  getUrlpara : function(url, para) {
    var name, value;
    //取得整个地址栏，因为测试需要，现在用的自定义的url，正式环境，可以把var str=location.href;放开，把var str = url;隐藏或者删掉，上面的url声明也可以没用
    
    var str = url;
    var num = str.indexOf("?")
        str = str.substr(num + 1);
    var arr = str.split("&");
    console.log(arr)
    var newArr = [];
    for(var i = 0; i<arr.length; i++) {
  var nnum = arr[i].indexOf("=");
  newArr.push(arr[i].substring(0, nnum));
  newArr.push(arr[i].substr(nnum + 1));
}
var paraIndex = newArr.indexOf(para);
if (paraIndex >= 0) {
  return newArr[paraIndex + 1];
} else {
  return para + "不是正确的参数名"
}
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    this.setData({
      isLogin: wx.getStorageSync('Token') ? true : false
    })
    if (options.q) {
      console.log(1111)
      var phone, id;
      let q = decodeURIComponent(options.q);
      // let result=this.getUrlpara(q,"phone")
      let resultId = that.getUrlpara(q, "id")
      let result = q.split('?')[1].split('=')[1].substring(0, 11);
      // console.log(result)

      // let resultId = q.split('=')[1];
      console.log(resultId)
      that.setData({
        // mobile: result,
        id: resultId
        // phone: result
      })
    }
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: function (res) {
            that.setData({
              show:false,
              name: res.userInfo.nickName,
              avatarUrl: res.userInfo.avatarUrl
            })
              // var test = "https://rental.homhere.cn/lookphonenumber?&id=34567"
              if (options.q) {
                console.log(1111)
                var phone, id;
                let q = decodeURIComponent(options.q);
                // let result=this.getUrlpara(q,"phone")
                 let resultId = that.getUrlpara(q, "id")
                 let result = q.split('?')[1].split('=')[1].substring(0,11);
                // console.log(result)
               
                // let resultId = q.split('=')[1];
                console.log(resultId)
                that.setData({
                  // mobile: result,
                  id: resultId
                  // phone: result
                })
                // console.log("1111",that.data.id)
                wx.login({
                  success(res) {
                    if (res.code) {
                      //发起网络请求
                      wxPromise.wxGet(API.HOMERENTALIFRENTALSTATUS, {
                        id: that.data.id,
                        code: res.code,
                        nickname: that.data.name,
                        avatar: that.data.avatarUrl
                      })
                        .then(res => {
                          console.log(res)
                          if (res.data.code == 1) {
                            that.setData({
                              phone: res.data.data.mobile
                            })
                            console.log()
                          } else if (res.data.code == 102) {
                            wx.showModal({
                              title: '提示',
                              content: '该房源已下架',
                              showCancel: false,
                              success: function (res) {
                                if (res.confirm) {
                                  wx.reLaunch({
                                    url: '/pages/index/index',
                                  })
                                }
                              }
                            })
                          } else if (res.data.code == 101) {
                            wx.reLaunch({
                              url: '/pages/rentOut/rentOut?id=' + that.data.id,
                            })
                          }
                        })
                    } else {
                      console.log('登录失败！' + res.errMsg)
                    }
                  }
                })
              }
            }
          })
        }
      }
    })
    
  
  },
  copy(){
    wx.setClipboardData({
      data: this.data.phone.toString(),
      success: function (res) {
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})