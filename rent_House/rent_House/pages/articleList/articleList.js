const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page:1
  },
  getArticleList() {
    wxPromise.wxGet(
      API.HOMERENTALARTICLE_LIST,
      {
        page:this.data.page
      }
    ).then(res=> {
      let arr = [];
      for (let i = 1; i <= res.data.data.last_page;i++){
        arr.push(i);
      }
      if(res.data.code == 1){
        this.setData({
          portal: res.data.data.data,
          staticPage: arr 
        })
      }
    })
  },
  // 分页器
  // prev 上一页
  prev() {
    if (this.data.page <= 1) return;
    this.setData({
      page: this.data.page - 1
    })
    this.getArticleList();
  },
  // 选中哪一页
  selectPage(event) {
    this.setData({
      page: event.currentTarget.dataset.index
    })
    this.getArticleList();
  },
  // 下一页
  next() {
    if (this.data.page >= this.data.last_page) return;
    this.setData({
      page: this.data.page + 1
    })
    this.getArticleList();
  },
  // 获取文章详情页
  getArticleDetail(event) {
    wx.navigateTo({
      url: '../../selfer/articleDetail/articleDetail?id=' + event.currentTarget.id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      host :API.COMMON
    })
    this.getArticleList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})