 // pages/reportSuccess/index.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    formId:''

  },
  registerFormSubmit: function (e) {
    //    打印formId
    var id = wx.getStorageSync('openid');
    this.setData({
      formId: e.detail.formId
    })
    wxPromise.wxGet(API.USERPUBLICGETCONFIG, {
            openid:id,
            formid: e.detail.formId
          })
            .then(res => {
              console.log(res)
              wx.reLaunch({
                url: '/pages/index/index',
              })
            }) 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.showModal({
    //   title: '您的帖子已发布成功',
    //   content:"1、当天发的房源，公众号次日统一发布<br/>2、公众号发布时间：每个工作日和周六，下午发布（节假日发布时间待定）",
    //   icon: 'success',
    //   duration: 2000000000
    // })
    // wx.showModal({
    //   title: '房源发布',
    //   content: '发布成功',
    //   showCancel:false,
    //   success:function(res){
    //     if(res.confirm){
    //       wxPromise.wxGet(API.USERPUBLICGETCONFIG,{
    //         openid:id,
    //         formid:12
    //       })
    //         .then(res => {
    //           console.log(res)
    //         }) 
    //     }
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})