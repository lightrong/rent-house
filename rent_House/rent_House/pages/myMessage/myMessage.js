// pages/myMessage/myMessage.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: 1,
    indexFor:[1,1,1,1,1,1,1,1,1,1,1,11],
    isStatus:false,
    isXTStatus:false
  },
  //跳到首页
  toHome:function(){
    wx.switchTab({
      url: '../index/index',
    })
  },
  getMessage(){
    wxPromise.wxPost(API.USERRENTALGETMESSAGE, {}, wx.getStorageSync('Token'))
      .then(res => {
        for (var i = 0; i < res.data.data.length;i++){
          if (res.data.data[i]["status"]==0){
            this.setData({
              isStatus:true
            })
          }
        }
        this.setData({
          messageList: res.data.data
        })
      })
  },
  getXTMessage(){
    wxPromise.wxPost(API.USERRENTALGETMESSAGE, { admin: 123 }, wx.getStorageSync('Token')) 
      .then(res => {
        for (var i = 0; i < res.data.data.length; i++) {
          if (res.data.data[i]["status"] == 0) {
            this.setData({
              isXTStatus: true
            })
            break;
          }else{
            this.setData({
              isXTStatus: false
            })
          }
        }
        this.setData({
          messageXTList: res.data.data
        })
      })
  },
  //  动画函数
  selectType(event) {
    this.animation.translateX(event.currentTarget.dataset.distance).step();
    this.setData({
      xtleft:false,
      animationData: this.animation.export(),
      type: event.currentTarget.dataset.type
    })
    this.getMessage();
    this.getXTMessage();
    // if (event.target.dataset.type==2){
    // }else{
    // }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease-out',
    })
    this.getMessage();
    this.getXTMessage();
    if (options.type) {
      this.setData({
        type: 2,
        xtleft:true
      })
      this.getXTMessage();
    }else{
      
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})