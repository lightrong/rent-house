const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
const toast = require('../../utils/toast.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据 
   */
  data: {
    imgIndex:[1,2,3,4,5,6,7,8,9,10],
    homeDetail:'',
    comment:[],
    starCount:5,
    labelList:[],
    homeId:'',
    currentArea:'',
    host:'',
    showLoading:true,
    contact:'',
    // 搜索内容
    title:'',
    indicatorDots: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    focus:false,
    // 地址
    region : [],
    total:0,
    page:0,
    star:5,
    myself:true,
    // 海报相关参数
    showPoster:false,
    rpx:1,
    chuzuPoster: "https://rental.homhere.cn/poster/chuzu.jpg",
    qiuzuPoster: "https://rental.homhere.cn/poster/qiuzu.jpg",
    posterImg:'',
    thumbArr:[],
    houseType:1,
    noHouseList: false
  },
  fabuBtn() {
    wx.switchTab({
      url: '/issue/issue/issue',
    })
  },
  changeData: function () {

    var options = { 'isLogin': this.data.isLogin }

    this.onLoad(options);//最好是只写需要刷新的区域的代码，onload也可，效率低，有点low
  },
  // 地区选择
  regionchange(e) {
    console.log(e.type);
  },

  // 举报
  getReport(){
    wx.navigateTo({
      url: '../report/report?homeId=' + this.data.homeId,
    })
  },

  // 获取用户信息
  getUserInfo() {
    wx.navigateTo({
      url: '../userInfo/userInfo?userid=' + this.data.homeDetail.uid,
    })
  },
  // 获取配置参数
  getconfig() {
    wxPromise.wxGet(API.USERPUBLICGETCONFIG)
    .then(res => {
      this.setData({
        labelList: res.data.data.rent_comment_tag
      })
    })
  },
  // 获取详情的数据
  getHomeDetail(page = 1 ,fn) {
    wxPromise.wxPost(API.APIHOMERENTALRENTING_DETAILS,
    {
      id:this.data.homeId,
      page
    },
    wx.getStorageSync('Token'))
   
    .then(res => {
 console.log(res)
      let arr = [];
      let data = res.data.data.last_page
      for (let i = 1; i <= data; i++) {
        arr.push(i);
      }

      // 判断房源是否空
      if (res.data.msg != "ok") {
       let that=this
        setTimeout(function () {
          wx.switchTab({
            url: '../index/index',
          })
          },3000)
        setTimeout(function () {
         
          that.setData({
            noHouseList: true
          })
        }, 1000)
      }   else{

      if (res.data.data.list.mobile  == wx.getStorageSync("phone")){
        this.setData({
          myself:true
        })
      }
      var avatarList=[]
      for (var i = 0; i < res.data.data.contact_avatar.length;i++){
        avatarList[i] = res.data.data.contact_avatar[i].avatar
      }
      this.setData({
        avatarList: avatarList,
        showLoading:false,
        contact_avatar: res.data.data.contact_avatar,
        homeDetail:res.data.data.list,
        comment:res.data.data.comment,
        total: res.data.data.total,
        commentArr:arr,
        comment_average:res.data.data.comment_average,
        houseType: res.data.data.list.type
      })
      // 获取缩略图
      // if (this.data.houseType !=2) {
      //   this.getThumb();
      // }
      if(typeof fn == 'function'){
        fn(res);
        }
      }
      // this.commentContent.readyComplete();
    })
  },
  // 检测到未登录——跳转到登陆页
  nacToLogin:function(){
    wx.navigateTo({
      url: '/selfer/login/login',
    })
  },
  // 获取城市
  getPosition(event) {
    this.setData({
      currentArea: event.detail.value[1]
    })
  },
  //  图片预览
  previewImg(event){
    let urls = []; 
    this.data.homeDetail.thumb.forEach((value,index)=>{
      urls.push(value);
    })
    wx.previewImage({
      current: event.currentTarget.dataset.src, // 当前显示图片的http链接
      urls:urls // 需要预览的图片http链接列表
    })
  },

  // 点赞
  like() {
    if(this.data.isLogin){
      wxPromise.wxPost(API.USERRENTALLIKE, {
        id: this.data.homeDetail.id
      }, wx.getStorageSync('Token'))
        .then(res => {
          if (res.data.code == 1) {
            wx.showToast({
              title: res.data.msg,
              icon: 'none'
            })
            this.getHomeDetail();
          } else {
            toast.toast(res.data.msg);
          }
        })
    }else{
      this.nacToLogin();
    }
  },
  // 踩
  dislike() {
    if(this.data.isLogin){
      wxPromise.wxPost(API.USERRENTALDISLIKE, {
        id: this.data.homeDetail.id
      }, wx.getStorageSync('Token'))
        .then(res => {
          if (res.data.code == 1) {
            wx.showToast({
              title: res.data.msg,
              icon: 'none'
            })
            this.getHomeDetail();
          } else {
            toast.toast(res.data.msg);
          }
        })
    }else{
      this.nacToLogin();
    }
  },
  // 收藏
  collect() {
    wxPromise.wxPost(API.USERRENTALCOLLECT,{
      id:this.data.homeDetail.id
    },
    wx.getStorageSync('Token'))
    .then(res => {
      if(res.data.code == 1) {
        wx.showToast({
          title: res.data.msg,
          icon:'none'
        })
        this.getHomeDetail();
      }else {
        toast.toast(res.data.msg);
      }
    })
  },
  //  获取联系方式
  getContact () {
    if(this.data.isLogin){
      if (this.data.contact) {
        wx.showActionSheet({
          itemList: ['拨打电话', '复制'],
          success: res => {
            if (res.tapIndex == 0) {
              wx.makePhoneCall({
                phoneNumber: this.data.contact //仅为示例，并非真实的电话号码
              })
            }
            else {
              wx.setClipboardData({
                data: this.data.contact,
                success: function (res) {
                  wx.showToast({
                    title: '复制成功！',
                  })
                }
              })
            }
          }
        })
      }
      else {
        wxPromise.wxPost(API.USERPUBLICVIEWCONTACT, {
          id: this.data.homeDetail.id
        }, wx.getStorageSync('Token'))
          .then(res => {
            if (res.data.code == 1) {
              wx.showToast({
                title: '操作成功',
                icon: 'none'
              })
              this.setData({
                contact: res.data.data.mobile
              })
            }
            else if (res.data.code == 0) {
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
            }
            else if (res.data.code == 10001) {
              toast.toast(res.data.msg);
            }
            else {
              wx.showToast({
                title: '服务器异常',
                icon: 'none'
              })
            }
          })
      }
    }else{
      this.nacToLogin()
    }
  },
  // 组件渲染完成 
  readyEvent() {
    this.commentContent = this.selectComponent("#comment");
  },
  // 评论组件
  _comment(event) {
    this.getHomeDetail(1,res=> {
      if(this.commentContent.data.showMore) {
        this.commentContent.setData({
          commentCount: res.data.data.comment.length
        })
      }
    });
    this.setData({
      starCount:5
    })
  },
  _pageEvent(event) {
    this.getHomeDetail(event.detail.page);
  },
  // 获取搜索内容
  getTitle (event) {
    this.setData({
      title:event.detail.value
    })
  },
  // 搜索内容 
  searchHouse () {
    wx.navigateTo({
      url: '../searchList/searchList?title=' + this.data.title,
    })
  },
  // 点击评论
  tapComment() {
    wx.createSelectorQuery().select('#rentOut_container').boundingClientRect( rect=> {
      // 使页面滚动到底部
      wx.pageScrollTo({
        scrollTop: rect.bottom
      })
      // 设置获取焦点
      setTimeout(()=>{
        this.setData({
          focus:true
        })
      }, 350)
    }).exec()
  },
  // 生成海报
  getPoster () {
    if(this.data.isLogin){
      wx.showLoading({
        title: '海报生成中...',
      })
      wxPromise.wxPost(
        API.HOMERENTALCREATEPOESTER,
        { id: this.data.homeId }
      ).then(res => {
        if (res.data.code == 1) {
          wx.getImageInfo({
            src: this.data.host + res.data.data,
            success: path => {
              this.setData({
                posterImg: path.path,
                showPoster: true
              })
              wx.hideLoading();
            }
          })
        }
        else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
          wx.hideLoading();
        }
      })
        .catch(err => {
          wx.showToast({
            title: '服务器繁忙',
            icon: 'none'
          })
          wx.hideLoading();
        })
    }else{
      this.nacToLogin();
    }
  },
  // 点击海报
  posterTap() {
    this.getPoster();
    // this.createdCode(this.data.rpx);
  },
  
  // 保存按钮
  savePoster() {
    wx.showLoading({
      title: '正在保存...',
    })
    let that = this;
    // 获取用户是否开启用户授权相册
    wx.getSetting({
      success(res) {
        // 如果没有则获取授权
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
              wx.saveImageToPhotosAlbum({
                filePath: that.data.posterImg,
                success() {
                  wx.showModal({
                    content: '图片已保存到相册，赶紧晒一下吧~',
                    showCancel: false,
                    confirmText: '好的',
                    confirmColor: '#333',
                  })
                },
                fail() {
                  wx.showToast({
                    title: '保存失败',
                    icon: 'none'
                  })
                },
                complete() {
                  that.setData({
                    showPoster: false
                  })
                  wx.hideLoading();
                }
              })
            },
            fail() {
              // 如果用户拒绝过或没有授权，则再次打开授权窗口
              //（ps：微信api又改了现在只能通过button才能打开授权设置，以前通过openSet就可打开，下面有打开授权的button弹窗代码）
              that.setData({
                openSet: true
              })
              wx.hideLoading();
            }
          })
        } else {
          // 有则直接保存
          wx.saveImageToPhotosAlbum({
            filePath: that.data.posterImg,
            success() {
              wx.showModal({
                content: '图片已保存到相册，赶紧晒一下吧~',
                showCancel: false,
                confirmText: '好的',
                confirmColor: '#333',
              })
            },
            fail() {
              wx.showToast({
                title: '保存失败',
                icon: 'none'
              })
            },
            complete() {
              that.setData({
                showPoster:false
              })
              wx.hideLoading();
            }
          })
        }
      }
    })
  },
  // 取消授权
  cancleSet() {
    this.setData({
      openSet: false
    })
  },
  
  // 返回主页
  goHome() {
    wx.switchTab({
      url: '../index/index',
    })
  },
  // 关闭海报
  closePoster() {
    this.setData({
      showPoster:false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isLogin: wx.getStorageSync('Token')?true:false
    })
    this.getconfig();
    if(options.q) {
      let q = decodeURIComponent(options.q);
      let result = q.split('?')[1].split('=')[1];
      this.setData({
        homeId: result,
        host: API.COMMON,
        currentArea: app.globalData.currentCity|| '深圳市',
        region: app.globalData.region || []
      })
    }
    else {
      this.setData({
        homeId : options.id,
        host:API.COMMON,
        currentArea: app.globalData.currentCity || '深圳市',
        region:app.globalData.region || []
      })
    }
    // 获取详情
    this.getHomeDetail(); 
    // 设置rpx
    try {
      const res = wx.getSystemInfoSync()
      this.setData({
        rpx: res.windowWidth / 375
      })
    } catch (e) {
      console.log(e);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    return {
      title: this.data.homeDetail.title,
      path: '/pages/rentOut/rentOut?id=' + this.data.homeDetail.id
    }
  }
})