// pages/myMessageContent/myMessageContent.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    myId: wx.getStorageSync("id"),
    page: 1,
    contentList: [{
        time: '01-24 03:55',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '这个空调可以便宜点嘛？',
        isowner: 0
      },
      {
        time: '',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '空调可以便宜',
        isowner: 1
      },
      {
        time: '11-27 1 4:12',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈',
        isowner: 0
      },
      {
        time: '',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈',
        isowner: 0
      },
      {
        time: '11-27 14:20',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈',
        isowner: 1
      },
      {
        time: '',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈',
        isowner: 1
      },
      {
        time: '',
        avatar: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2090142012,457058953&fm=26&gp=0.jpg',
        content: '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈',
        isowner: 0
      },
    ],
    listshow: false,
    houseinfo: '',
    checkcode: '',
    codeSuccess: '',
    checkmsg: '',
    btnColor: false,
    sendPar: {},
    host:'',
    cursorSpacing:""
    // housePicture:'',
   
  },
  // 当code=0时弹出框
  motai: function() {
    wx.showModal({
      title: '温馨提示',
      content: this.data.checkmsg,
      confirmText: '去发布',
      confirmColor: '#576a94',
      success: function(res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '../../issue/issueRentOut/issueRentOut'
          })
        } else if (res.cancel) {

        }
      }
    })
  },
  // 组件checkcode传参
  onMyEvent: function(e) {
    if (e.detail.codeSuccess === 123) {
      //验证成功
      this.sendMessage(this.data.sendPar)
    }
    this.setData({
      codeSuccess: e.detail.codeSuccess
    })

  },
  //点击房源框
  houseDetails: function(e) {
    wx.navigateTo({
      url: '../../pages/rentOut/rentOut?id=' + e.currentTarget.dataset.id,
    })
  },
  // 房源列表按钮
  listHouse: function() {
    wxPromise.wxGet(API.USERRENTALGETRENTALLIST, {}, wx.getStorageSync('Token')).then(result => {
      console.log(result)
      this.setData({
        houseinfo: result.data.data
      })
    })
    this.setData({
      listshow: true
    })
  },
  //关闭房源列表按钮
  closeList: function() {
    this.setData({
      listshow: false
    })
  },
  //数组排序
  compare: function(property) {
    return function(a, b) {
      var value1 = a[property];
      var value2 = b[property];
      return value1 - value2;
    }
  },

  // 发送
  sendOut: function(res) {
    console.log(res)
    let returncode = this.data.codeSuccess //验证码返回值
    let index = res.currentTarget.dataset.index
    wxPromise.wxPost(API.USERRENTALSENDMESSAGE, {
      type: 2,
      uid: this.data.otherId,
      message: this.data.houseinfo[index].id,
      // form_id: res.detail.formId
    }, wx.getStorageSync('Token')).then(result => {
      this.getMessage()
    })
    this.setData({
      listshow: false
    })
  },

  sendSubmit: function(res) {

    this.checkCodeSend()
    let sendPar = {
      messageContent: res.detail.value.messageContent,
      formId: res.detail.formId
    }
    this.setData({
      sendPar
    })
    console.log(this.data.checkcode)
    if (this.data.checkcode == 1) {
      //直接发送
      this.sendMessage(sendPar)
    } else if (this.data.checkcode == 2) {
      //需要验证
      if (this.data.codeSuccess === 123) {
        this.sendMessage(sendPar)
      } else {
        this.selectComponent('#checkcode').showDialogBtn();
      }
    } else {
      if (this.data.checkmsg != "checkCode") {
        //不能发送
        this.motai()
      } else {
        if (this.data.codeSuccess === 123) {
          this.sendMessage(sendPar)
        } else {
          this.selectComponent('#checkcode').showDialogBtn();
        }
      }
    }

  },
  sendMessage(sendPar) {
    if (!sendPar.messageContent) {
      console.log('输入不能为空')
    } else {
      this.setData({
        message: ''
      })
      wxPromise.wxPost(API.USERRENTALSENDMESSAGE, {
        uid: this.data.otherId,
        message: sendPar.messageContent,
        form_id: sendPar.formId
      }, wx.getStorageSync('Token')).then(result => {
        this.getMessage()
      })
    }

  },
  getMessage() {
    let that=this
    wxPromise.wxPost(API.USERRENTALMESSAGEDETAILS, {
      page: 1,
      id: this.data.otherId
    }, wx.getStorageSync('Token')).then(res => {

      console.log(res.data.data.data)
      res.data.data.data.forEach(item=>{
        if (item.message.thumb){
          item.message.thumb=item.message.thumb.split('|')[0]
          // console.log(item.message.thumb.split('|'))
        }
      })
      console.log(res.data.data.data)
      this.setData({
        scrollTop: (res.data.data.data.length * 75) + 30,
        contentListM: res.data.data.data.sort(this.compare('id')),
        send_avatar: res.data.data.send_avatar,
        user_avatar: res.data.data.user_avatar,
        // housePicture: res.data.data.data[4].message.thumb.split('|')
      })
      // console.log(this.data.housePicture)
    })
  },
  getMessagePage(pages) {
    wxPromise.wxPost(API.USERRENTALMESSAGEDETAILS, {
      page: pages,
      id: this.data.otherId
    }, wx.getStorageSync('Token')).then(res => {
      if (res.data.code == 1) {
        this.setData({
          contentList: [...(res.data.data.data.reverse()), ...this.data.contentList],
          total: res.data.data.last_page
        })
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
    console.log(res)
  },
  loading(res) {
    if (this.data.page == this.data.total) return;
    this.setData({
      page: this.data.page + 1
    })
    this.getMessagePage(this.data.page)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  checkCodeSend: function() {
    wxPromise.wxGet(API.USERRENTALCHECKMESSAGE, {
      uid: this.data.otherId,
    }, wx.getStorageSync('Token')).then(result => {
      this.setData({
        checkcode: result.data.code,
        checkmsg: result.data.msg,

      })
      if (result.data.code == 0 && result.data.msg != "checkCode") {
        this.motai()
        this.setData({
          btnColor: true
        })
      }
    })
  },
  focus:function(){
    let that = this
    wx.getSystemInfo({
      success: function (res) {
        console.log(res)
        if (res.brand == "iPhone") {
          that.setData({
          bottom:40
          })
        } else {
          that.setData({
            bottom: 0
          })
        }
      }
    })
  },

  onLoad: function(options) {
    let that = this
    wx.getSystemInfo({
      success: function (res) {
        console.log(res)
        if (res.model.indexOf("iPhone X") >= 0 || res.model.indexOf("iPhone 11") >= 0) {
          that.setData({
            bottom: 40
          })
        } else {
          that.setData({
            bottom: 0
          })
        }
      }
    })
    this.setData({
      otherId: options.id,
      host:API.COMMON
    })
    this.getMessage()
    this.checkCodeSend()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})