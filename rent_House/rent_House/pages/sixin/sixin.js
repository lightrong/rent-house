// pages/sixin/sixin.js
const API = require('../../utils/API.js');
const wxPromise = require('../../utils/promise.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgIndex: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    checkmsg: '',
    btnColor: false,
    checkcode: '',
    codeSuccess: '',
    sendPar: {},
    newMessage:0
  },

  // 组件checkcode传参
  onMyEvent: function(e) {
    console.log('HEllow')
    console.log(e)
    if (e.detail.codeSuccess === 123) {
      this.sendMessage(this.data.sendPar)
    }
    this.setData({
      codeSuccess: e.detail.codeSuccess
    })

  },
  // 弹出框
  motai: function() {
    wx.showModal({
      title: '温馨提示',
      content: this.data.checkmsg,
      confirmText: '去发布',
      confirmColor: '#576a94',
      success: function(res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '../../issue/issueRentOut/issueRentOut'
          })
        } else if (res.cancel) {

        }
      }
    })
  },
  sendSubmit: function(res) {
    let sendPar = {
      messageContent: res.detail.value.messageContent,
      formId: res.detail.formId
    }
    this.setData({
      sendPar
    })
    if (this.data.checkcode==0) {
      if (this.data.checkmsg != "checkCode"){
        this.motai()
      }else{
        console.log('HEllo')
        if (this.data.codeSuccess == 123) {
          this.sendMessage(sendPar)
        } else {
          this.selectComponent('#checkcode').showDialogBtn();
        }
      }
    } else {
      if (this.data.checkcode==2) {
        if (this.data.codeSuccess == 123) {
          this.sendMessage(sendPar)
        } else {
          this.selectComponent('#checkcode').showDialogBtn();
        }
      } else {
        this.sendMessage(sendPar)
      }
    }
  },
  sendMessage(sendPar) {
    if (!sendPar.messageContent.length) {
      wx.showToast({
        title: "内容不能为空",
        icon: "none"
      })
    } else {
      this.setData({
        message: ''
      })
      wxPromise.wxPost(API.USERRENTALSENDMESSAGE, {
        uid: this.data.uid,
        message: sendPar.messageContent,
        form_id: sendPar.formId
      }, wx.getStorageSync('Token')).then(result => {
        wx.showToast({
          title: result.data.msg,
          icon: "none"
        })
      })
    }
  },

  //跳转到消息列表
  gotomessage:function(){ 
    var that=this
    wx.navigateTo({
      url: '/pages/myMessage/myMessage',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this
    wxPromise.wxPost(API.USERRENTALGETMESSAGESTATUS
      ,{uid: wx.getStorageSync("id")},wx.getStorageSync('Token')).then(result => {
        if (result.data.code == 1) {
          that.setData({
            newMessage: result.data.data.new_message
          })
        }
      })
    wxPromise.wxGet(API.USERRENTALCHECKMESSAGE, {
      uid: options.uid
    }, wx.getStorageSync('Token')).then(result => {
      console.log(result)
      this.setData({
        checkmsg: result.data.msg,
        checkcode: result.data.code
      })
      if (result.data.code == 0 && result.data.msg != "checkCode") {
        that.motai()
        that.setData({
          btnColor: true
        })
      } 
    })
    // var that = this;
    that.setData({
      uid: options.uid,
      contact_avatar: options.contact_avatar.split(",")
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})