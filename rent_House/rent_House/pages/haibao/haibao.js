// pages/haibao/haibao.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgPath:"https://rental.homhere.cn/poster/chuzu.jpg",
    imgPath2:"https://rental.homhere.cn/poster/qiuzu.jpg",
    appshow:false
  },
  //文本换行 参数：1、canvas对象，2、文本 3、距离左侧的距离 4、距离顶部的距离 5、6、文本的宽度
  drawText: function (ctx, str, leftWidth, initHeight, titleHeight, canvasWidth) {
    var lineWidth = 0;
    var lastSubStrIndex = 0; //每次开始截取的字符串的索引
    for (let i = 0; i < str.length; i++) {
      lineWidth += ctx.measureText(str[i]).width;
      if (lineWidth > canvasWidth) {
        ctx.fillText(str.substring(lastSubStrIndex, i), leftWidth, initHeight); //绘制截取部分
        initHeight += 20; //20为字体的高度
        lineWidth = 0;
        lastSubStrIndex = i;
        titleHeight += 30;
      }
      if (i == str.length - 1) { //绘制剩余部分
        ctx.fillText(str.substring(lastSubStrIndex, i + 1), leftWidth, initHeight);
      }
    }
    // 标题border-bottom 线距顶部距离
    titleHeight = titleHeight + 10;
    return titleHeight
  },
  saveShareImg: function () {
    var that = this;
    wx.showLoading({
      title: '正在保存',
      mask: true,
    })
    setTimeout(function () {
      wx.canvasToTempFilePath({
        canvasId: that.data.appshow ? "canvas" :"canvas2",
        success: function (res) { 
          wx.hideLoading();
          var tempFilePath = res.tempFilePath;
          wx.saveImageToPhotosAlbum({
            filePath: tempFilePath,
            success(res) {
              wx.showModal({
                content: '图片已保存到相册，赶紧晒一下吧~',
                showCancel: false,
                confirmText: '好的',
                confirmColor: '#333',
                success: function (res) {
                  if (res.confirm) { }
                },
                fail: function (res) { }
              })
            },
            fail: function (res) {
              wx.showToast({
                title: res.errMsg,
                icon: 'none',
                duration: 2000
              })
            }
          })
        }
      });
    }, 1000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.downloadFile({
      url: that.data.imgPath,
      success: function (res) {
        that.setData({
          imgPath: res.tempFilePath
        })
      },
      fail: function (err) {
        console.log(err);
      },
    });
    wx.request({
      url: 'https://rental.homhere.cn/api/user/rental/erweima?id=' + options.id,
      header: {
        "XX-Device-Type": "wxapp",
        "XX-Token": wx.getStorageSync('Token'),
        'content-type': 'application/x-www-form-urlencoded' },
      method: 'POST',
      data: {},
      success:function(res){
        var erweima = "https://rental.homhere.cn" + res.data.data.qr_url;
        wx.downloadFile({
          url: erweima,
          success: function (res) {
            that.setData({
              erweima: res.tempFilePath
            });
            wx.request({
              url: 'https://rental.homhere.cn/api/home/rental/renting_details?id=' + options.id,
              header: { "content-type": "application/x-www-form-urlencoded" },
              method: 'POST',
              data: {},
              success: function (res) {
                that.setData({
                  appshow: res.data.data.list.type_str == "个人求租" ? false : true
                });
                if (that.data.appshow) {
                  var area = res.data.data.list.area;
                  var subway = res.data.data.list.subway == undefined ? res.data.data.list.subway : "";
                  var monthly = res.data.data.list.monthly;
                  var title = res.data.data.list.title;
                  var release_time = res.data.data.list.release_time;
                  var thumb = res.data.data.list.thumb;
                  var desc = res.data.data.list.desc;
                  var str1 = area + subway + title
                  str1 = str1.replace(/\s+/g, '');
                  wx.getSystemInfo({
                    success: function (e) {
                      var context = wx.createCanvasContext('canvas');
                      context.drawImage(that.data.imgPath, 0, 0, e.windowWidth, e.windowHeight);
                      context.setFontSize(16);
                      context.fillStyle = "#000";
                      // context.fillText("出租版", 20, 20);
                      context.fillText(str1, 20, 45, 350);
                      context.fillText(release_time, 20, 70, 350);
                      context.fillStyle = "#FF9B1B";
                      context.fillText("￥" + monthly, 278, 75, 76);
                      if (thumb.length >= 4) {
                        for (var i = 0; i < 4; i++) {
                          thumb[i] = "https://rental.homhere.cn" + thumb[i];
                        }
                        for (var i = 0; i < 4; i++) {
                          wx.downloadFile({
                            url: thumb[i],
                            success: function (res) {
                              thumb[i] = res.tempFilePath;
                            },
                            fail: function (err) {
                              console.log(err);
                            },
                          });
                        }
                        setTimeout(function(){
                          context.drawImage(thumb[0], 17, 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[1], (17 + (e.windowWidth / 2 - 20) + 5), 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[2], 17, 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[3], (17 + (e.windowWidth / 2 - 20) + 5), 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                        },1500);

                      } else if (thumb.length >= 2) {
                        for (var i = 0; i < 2; i++) {
                          thumb[i]= "https://rental.homhere.cn" + thumb[i];
                        }
                        for (var i = 0; i < 2; i++){
                          wx.downloadFile({
                            url: thumb[i],
                            success: function (res) {
                              thumb[i] = res.tempFilePath;
                            },
                            fail: function (err) {
                              console.log(err);
                            },
                          });
                        }
                        setTimeout(function () {
                          context.drawImage(thumb[0], 17, 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[1], (17 + (e.windowWidth / 2 - 20) + 5), 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[0], 17, 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[1], (17 + (e.windowWidth / 2 - 20) + 5), 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                        }, 1500);

                      } else {
                        var thumbPath = "https://rental.homhere.cn" + thumb[0];
                        wx.downloadFile({
                          url: thumbPath,
                          success: function (res) {
                            thumb[0] = res.tempFilePath;
                          },
                          fail: function (err) {
                            console.log(err);
                          },
                        });
                        setTimeout(function () {
                          context.drawImage(thumb[0], 17, 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[0], (17 + (e.windowWidth / 2 - 20) + 5), 85, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[0], 17, 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                          context.drawImage(thumb[0], (17 + (e.windowWidth / 2 - 20) + 5), 85 + 105 + 5, (e.windowWidth / 2 - 20), 105);
                        }, 1500);
                      }
                      setTimeout(function(){
                        context.fillStyle = "#000";
                        that.drawText(context, desc, 25, 335, 148, 325);
                        context.drawImage(that.data.erweima, e.windowWidth / 2 - 50, 420, 100, 100);
                        context.draw();
                      },2000);
                    },
                  })
                } else {
                  var area = res.data.data.list.area;
                  var subway = res.data.data.list.subway == 'undefined' ? res.data.data.list.subway : "";
                  var monthly = res.data.data.list.monthly;
                  var title = res.data.data.list.title;
                  var desc = res.data.data.list.desc;
                  var str1 = area + subway + title
                  str1 = str1.replace(/\s+/g, '');
                  str1 = "                  " + str1;
                  wx.getSystemInfo({
                    success: function (e) {
                      var context = wx.createCanvasContext('canvas2');
                      wx.downloadFile({
                        url: that.data.imgPath2,
                        success: function (res) {
                          that.setData({
                            imgPath: res.tempFilePath
                          })
                        },
                        fail: function (err) {
                          console.log(err);
                        },
                      });
                      context.drawImage(that.data.imgPath2, 0, 0, e.windowWidth, e.windowHeight);
                      context.setFontSize(16);
                      context.fillStyle = "#000";
                      // context.fillText(str1, 120, 20, 350);
                      that.drawText(context, str1, 40, 33, 350, 330);
                      context.fillStyle = "#FF9B1B";
                      context.fillText("￥" + monthly, 270, 90, 76);
                      context.fillStyle = "#000";
                      that.drawText(context, desc, 25, 350, 148, 325);
                      context.drawImage(that.data.erweima, e.windowWidth / 2 - 50, 420, 100, 100);
                      context.draw();
                    },
                  })
                }

              },
              fail: function (err) {
                console.log(err);
              }
            })
          },
          fail: function (err) {
            console.log(err);
          },
        });
        // that.setData({
        //   erweima: "https://rental.homhere.cn" + res.data.data.qr_url
        // });
    
      },
      fail:function(err){
        console.log(err);
      },
    
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (e) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})